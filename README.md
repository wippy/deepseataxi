# README for Deep Sea Taxi #

### What is Deep Sea Taxi? ###

* Deep Sea Taxi is a student projects attempt for a remake of the 1984's classic "Space Taxi".

### Version ###
* 1.0

### History ###
* Well, as this is version 1.0 there is no history! We just gave birth to our 'baby'. Happy Birthday.. we love you. ;-)

### License ###

Deep Sea Taxi is covered by the [MIT license](https://opensource.org/licenses/MIT).

### Interested in more information? ###
Visit http://www.wippy.bplaced.net

### Who do I talk to? ###

* Talk to the admins by sending an e-mail:
    * [Stephan](mailto:stephan.karch@bg.fhdw.de)
    * [David](mailto:david.schmeisser@bg.fhdw.de)
    * [Phillip](mailto:developer@philliprohde.de)

### Focus areas by admin ###
* Coding: This game has been developed by all of us. If you really want to point out a person you could refer to Phillip as the "grandmaster".. however, each admin is capable to understand and explain each single line of code.
* Please have a look at the below mentioned points for __some special tasks__:
  * Movement, Collision, Game loop: David
  * Mapuploader, Preloader, World generation: Phillip
  * Sprites & Animations, Placement of "Nippes & Stuff", Exits, Moving Enemies: Stephan
* Design:
  * CSS & HTML, PHP: Phillip
  * Sprites, GFX, SFX: Stephan

### How do I get set up? ###

* Go to our specific [website](http://www.wippy.bplaced.net) and play!
* [Download](https://bitbucket.org/wippy/deepseataxi) or fork the master repository to your webserver and simply run the index.php.

### How to play? ###
* The overall goal of the game is to collect all divers in a proper chronological order and guide them out of the level with your submarine.
* The game itself will show you the correct order of the divers by numbers underneath them.
* To collect a diver guide the submarine overhead or beside the diver and land on the platform by using the 'sink'-option of the submarine. As soon as the submarine has successfully landed on the proper platform the diver will start to walk into the submarine. (no further action needed)
* The exits will open as soon as you have collected all divers.
* Guide the submarine into the exit. If there are more than one exit you can choose by yourself.
* Anyway.. givt attention to walls, underwater mines as well as 'octopussys'. Those things a more or less harmful and will lead to your virtual death ;-).

### Map upload ###
* We do offer the possibility to use maps created by your own. There are two ways to do this:
 * First by using and playing via the [website](http://www.wippy.bplaced.net). Use the "upload" function given by the homepage and play your map afterwards.
 * Second by [downloading](https://bitbucket.org/wippy/deepseataxi) the whole repository. Paste your map into the folder "maps" or by using the "mapupload" option on your (locally) running webserver.
* Please give attention to the well defined map criteria!

### Map criteria ###
##### 1. Defined properties: #####
   * Levels are based on a **grid with 32x24 (columns * rows)**
   * Files must end with **".map"**
   * **Exits** should consist of at least **3 symbols** of the type "E"
   * Platforms should be spaced by at least 3 symbols of the type "."
   * Do __not use more than one "T"__ (player starting point) and place it at a proper position.
   * Standard naming should be: levelXYZ.map where "XYZ" stands for three numbers:
     * X: Group ident number.
     * YZ: Map ident number; e.g. 01 first map, 02 second map, ...
   * Do only place divers onto platforms.

##### 2. Allowed symbols: ######
 * Please do **use capital letters only of the following type**:
     * "."  Sky, Space, Water.. this is open space.
     * "#"  Platform
     * "T"  Player starting point
     * "1,2,3,.."  Divers. Multiple Divers of same type possible
     * "E"  Exit. Each symbol opens a gate of 25px (use at least 3 symbols for each exit)
     * "X"  Static obstacle or enemy
     * "Y"  Moving enemy
     * "R"  Level border

* The project group allows up to three different divers per map. We do allow up to six different divers. However a larger number is not being supported.
* Each not known symbol will be replaced by "air".

### Known Bugs ###
* The hitbox of the submarine sprite (player) is not exactly rendered with the body itself. Therefore you might experience a collision at the edges of the sprite even when you do not have touched a collision object with the submarine body itself.
* It seems that we do have a pixel-error within the submarine sprite (player). However it appears seldom.. you might explode after collecting a diver. We gave our best but were not able to fix this issue. This is due the fact that we are blody amateurs. ;)
* Choosing an proper exit is a goood idea after having collected all divers. However, as you have chosen an exit please make sure to use the appropriate direction key prior entering the exit zone (e.g. right showing exit - use right direction key at last to enter exit zone)
* We did not block any keyboard functions so please give attention while playing the game. For example: Accidental reload of the homepage/game by pressing F5 is possible. This is just one of several things we could think of.
* Uncaught (in promise) DOMException: The play() request was interrupted by a call to pause(). (Chrome specific) We will fix this with the next version.
* Divers not being placed onto platforms are *not* collectable. Therefore it would be not possible to solve the level.
* Exits do need a size of at least 3 appropriate symbols. We check if there are at least 3 symbols on the map, but do not control if they are located in a proper way or not. * Plase make sure to colocate at least 3 symbols for an exit onto the map.
* Cannot sink when colliding with a moving enemy.
* Difficulty button doesn't show normal but flipped "Hard" in Safari.
* When uploading a map, two "Unexpected end of JSON input" errors appear in Chrome. The map gets still process correctly.
* After a map upload error appeared, you might be not able to upload again without a page reload.


### Credit ###
* The following images are **free assets**. They are marked for **free usage**. Covered by the following license: **Creative Common**
 * Voodoo Cactus Underwater Background:  Voodoo Cactus | [Online Source Link](http://opengameart.org/content/underwater-background-0) | License: Creative Commons Three 'CC3'
 * Underwater Mountains:                 PumpkinGlitters | [Online Source Link](http://pumpkingitters.deviantart.com) or [Online Source Link No.2](http://opengameart.org/content/underwater-mountains-version-1) | License: Creative Commons Three 'CC3'
 * Underwater Tileable:                  Scribe | [Online Source Link](http://opengameart.org/content/underwater-scene-loopable) | License: Creative Commons Three 'CC3'
 * Edge (Map surrounding):               Kenny Vleugs | [Online Source Link](http://www.kenny.nl) | License: Creative Commons Zero 'CC0'
 * Platforms (castle):                   Kenny Vleugs | [Online Source Link](http://www.kenny.nl) | License: Creative Commons Zero 'CC0'
 * Boxes (numbered & locked):            Kenny Vleugs | [Online Source Link](http://www.kenny.nl) | License: Creative Commons Zero 'CC0'
 * Transparent Bubble (animated):        alonarula | [Online Source Link](http://opengameart.org/content/transparent-bubble) | License: Creative Commons Zero 'CC0'
 * Gold Skull                            Gold Skull by Cameron 'cron' Fraser | [Online Source Link](http://opengameart.org/content/gold-skull) | License: CC-BY-SA 3.0
 * subMarineKill:                        Cuzco | [Online Source Link](http://opengameart.org/content/explosion) | License: Creative Commons Zero 'CC0'

* The following images are **free assets**. They are marked for **free, but non-commercial usage**.
 * Divers Sprite:                        Vashmaker "Vash" | [Online Source Link](http://vashmaker.blogspot.de/p/material.html) or [Online Source Link No.2](http://imgur.com/dXVrW92)
 * Octopus Sprite:                       ANgevon | [Online Source Link](http://www.gravity.co.kr)
 * Green Plant Sprite:                   ANgevon | [Online Source Link](http://rosprites.blogspot.com)
 * Seaweed Sprite:                       ANgevon | [Online Source Link](http://rosprites.blogspot.com)
 * Fishes Big & Fishes Small Sprite:     GrandmaDeb | [Online Source Link](http://forums.rpgmakerweb.com/index.php?/topic/30456-grannys-lists-vxace-animal-sprites/)

* The following images are either free and/or priced assets. They are all **covered by the same license** which can be viewed in the section **"About the license"** using the following [link](http://www.gamedeveloperstudio.com/about.php).
 * Brooks Underwater Background 1:       Robert Brooks - Gamedeveloperstudio | [Online Source Link](http://www.gamedeveloperstudio.com/graphics/viewgraphic.php?item=19411f5j2a3q1h5v38)
 * Brooks Underwater Background 2:       Robert Brooks - Gamedeveloperstudio | [Online Source Link](http://www.gamedeveloperstudio.com/graphics/viewgraphic.php?item=19411f5j2a3q1h5v38)
 * Fish Yellow Sprite:                   Robert Brooks - Gamedeveloperstudio | [Online Source Link](http://www.gamedeveloperstudio.com/graphics/viewgraphic.php?item=134a143f33661k3o0l)
 * Static & exploding Mine:              Robert Brooks - Gamedeveloperstudio | [Online Source Link](http://www.gamedeveloperstudio.com/about.php)
 * Submarine:                            Robert Brooks - Gamedeveloperstudio | [Online Source Link](http://www.gamedeveloperstudio.com/about.php)
 * Trash (normal & underwater Style):    Robert Brooks - Gamedeveloperstudio | [Online Source Link](http://www.gamedeveloperstudio.com/about.php)

* The following sounds are **free assets**. They are marked for **free usage**. Covered by the following license: **Creative Common**
 * underwaterMineExplosion:             (.mp3) Iwan Gabowitch | [Online Source Link](http://opengameart.org/content/synthesized-explosion) | License: Creative Commons ThreeZero 'CC0'
 * submarineKill:                       (.mp3) Blender Foundation | [Online Source Link](http://opengameart.org/content/big-explosion) | License: CC-BY 3.0
 * submarinePropeller                   (.mp3) iccleste | [Online Source Link](https://www.freesound.org/people/iccleste/sounds/260813/) | License: CC-BY 3.0
 * submarineSinkBeep                    (.mp3) jjastr1 | [Online Source Link](http://freesound.org/people/jjastr1/sounds/134947/) | License: CC-BY 3.0
 * heyYou (Attention FX):               (.mp3) Benboncan | [Online Source Link](https://www.freesound.org/people/Benboncan/sounds/77752/) | License: CC-BY 3.0
 * painOctopussy                        (.mp3) Michael Baradari | [Online Source Link](http://opengameart.org/content/11-male-human-paindeath-sounds) | License: CC-BY 3.0
 * underwaterAmbience1min               (.mp3) akemov | [Online Source Link](https://www.freesound.org/people/akemov/sounds/255597/) | License: CC-BY 3.0
 * underwaterAmbience3min               (.mp3) wjoojoo | [Online Source Link](https://www.freesound.org/people/wjoojoo/sounds/197751/) | License: CC-BY 3.0
 * klankbeeld_reverseAlarm              (.mp3) klankbeeld | [Online Source Link](sound from http://www.freesound.org/people/klankbeeld/) | License CC-BY 3.0

 Info: We used the following free online converter to convert some audio files into the .mp3-format. [Free Online Audio Converter (German)](http://online-audio-converter.com/de/)

### Intention ###
 **This is a study project. Its intention is entirely non-commercial!**
 **Please give us advice in case of any licensing conflict by using one of the abovementioned contact details. We respect such a case in each manner and remove or fix conflicts in no time (legitimate case).**
