/*
		@param anim        The explosion Animation itself
		@param audio			 The explosion sound.
		@param x					 x-coordinate.
		@param y					 y-coordinate.
		@param w					 The width.
		@param h					 The height.
*/
function Explosion(anim, audio, x, y, w, h)
{
    this.anim = anim;
    this.audio = audio;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.damage = 0;
    this.isSoundPlaying = false;

    this.shouldExplode = false;
    this.explosionIndex = 0;

    this.render = function()
    {
        //Only render if needed.
        if (this.shouldExplode)
        {
            this.anim.update(); //Update the animation
            this.anim.draw(this.x, this.y); //Draw the animation.

            /*
            		http://stackoverflow.com/questions/25654558/html5-js-play-same-sound-multiple-times-at-the-same-time
            		In order to play multiple instances of the sound at the same time, you will need to create a new copy
            		of that Audio element. One way to do this is to use the .cloneNode() method and play the cloned audio
            		every time Explosion() is being called and a sound should start!
            */
            if (!this.isSoundPlaying)
            {
                this.isSoundPlaying = true;
                this.audio.cloneNode(true).play();
            }
        }
    }
    this.getAnimation = function()
        {
            return this.anim;
        }
        //To avoid multiple explosions of the same object.
    this.isExploding = function()
    {
        return (this.shouldExplode) ? true : false;
    }
}
