/*
    Initialize basic values here.
*/
var caW = 800;
var caH = 600;
var rows = 0;
var cols = 0;

var errorCon;
var parsingError = false;
var parsingMessage = "<h2>Your map doesn't meet the required standards:</h2>";
var validExitFound = false;
var regDiver = /\d+$/; //Determine numbers in our map data.
var regName = /((?:(level)[a-z0-9]{3}(.map)))/; //"level" at the beginning, followed by three numbers and ".map"

/*
    parseMap will parse and format the data,
    as well as turning the data into a two-dimensonal
    array.

    @param fileName    The map name.
    @param fileData    The unformatted map data.
*/
function parseMap(fileName, fileData)
{
    parsingError = false;
    exitCounter = 0;
    taxiCounter = 0;
    errorCon = document.getElementById("error-message");
    errorCon.innerHTML = "";
    //Make sure that the level file is correctly named.
    if (!regName.test(fileName))
    {
        parsingError = true;
        parsingMessage += "<p>The file is incorrectly named.</p>";
    }
    var levelData = [];
    var lvl = fileData.replace(/\s+/g, "").split(""); //Remove whitespaces and line breaks & split into single characters
    var k = 0; //Counter for split array (lvl) position
    rows = fileData.split("\n").length;
    cols = ((fileData.replace(/\s+/g, "").split("").length) / rows); //Using replace("\n","") will result in an comma seperated number
    for (var i = 0; i < rows; i++)
    {
        levelData[i] = []; //Initialize 2nd dimension
        for (var j = 0; j < cols; j++)
        {
            levelData[i][j] = lvl[k];
            k++;
        }
    }
    generateThumbnail(fileName, fileData, levelData);
}

/*
    generateThumbnail draws the map on an invisible canvas
    and generates an image data stream out of it.
    After the generation, it appends the newly created image
    to the site.
    Error handling for required map stuff is included.

    @param fileName     The map name.
    @param rawMapData   The unformatted map data.
    @param tileMapData  The formatted, two-dimensonal map data.
*/
function generateThumbnail(fileName, rawMapData, tileMapData)
{
    //Create a new invisible canvas.
    var canvas = document.createElement("canvas");

    //Hide the canvas.
    canvas.style.display = "none";
    //HEHEHEHEHEHEHEHE YOU CAN'T SEE MEEEE

    //Changes done in order to solve your problem :-) - Thank you, Stephan! /PR
    canvas.width = 800; //Sets the canvas dimension width - Needed for tile calculation
    canvas.height = 600; //Sets the canvas dimension height - Needed for tile calculation
    canvas.style.width = '200'; //How wide should it get displayed in the browser?
    canvas.style.height = '150px'; //How tall should it get displayed in the browser?

    //Append the canvas to the body.
    document.body.appendChild(canvas);
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, caW, caH); //Clear it once.

    //Calculate tileWidth (25 = 800/24)) and tileHeight (25 = 600/32))
    tileWidth = (caW / cols);
    tileHeight = (caH / rows);

    //Initial handling
    var exitCounter = 0;
    var taxiCounter = 0;
    //Check if rows and columns fit the required numbers.
    if (cols != 32 || rows != 24)
    {
        parsingMessage += "<p>Grid is based on: 32*24 columns*rows! Please fix your grid.</p>";
        parsingError = true;
    }

    /*
      A basic variant of the map loader drawMap() loop.
      Draw simple colors for the necessary objects and leave out the rest.
      The error checks are implemented here.
    */
    for (var i = 0; i < rows; i++)
    {
        for (var j = 0; j < cols; j++)
        {
            //The handling using a switch is more efficient than using if's.
            switch (tileMapData[i][j])
            {
                case "R":
                    ctx.fillStyle = "#001742";
                    ctx.fillRect((j * tileWidth), (i * tileHeight), tileWidth, tileHeight); //j*25, i*25,25,25
                    break;
                case ".":
                    ctx.fillStyle = "#000000"; //Black, black, black are all my tiles..
                    ctx.fillRect((j * tileWidth), (i * tileHeight), tileWidth, tileHeight);
                    break;
                case "#":
                    ctx.fillStyle = "#001742"; //Dark blue
                    ctx.fillRect((j * tileWidth), (i * tileHeight), tileWidth, tileHeight);
                    break;
                case "E":
                    exitCounter++;
                    //Exit is located on the TOP or BOTTOM
                    if ((i == 0 || i == 23))
                    {
                        if (tileMapData[i][j + 1] == "E" && tileMapData[i][j + 2] == "E")
                        {
                            validExitFound = true;
                        }
                    }
                    //Exit is located on the LEFT or RIGHT side
                    if (((j == 0 || j == 31)))
                    {
                        if (tileMapData[i + 1][j] == "E" && tileMapData[i + 2][j] == "E")
                        {
                            console.log(tileMapData[i][j] + " " + tileMapData[i + 1][j] + " " + tileMapData[i + 2][j]);
                            validExitFound = true;
                        }
                    }
                    ctx.fillStyle = "#4D648F"; //Dark blue-ish
                    ctx.fillRect((j * tileWidth), (i * tileHeight), tileWidth, tileHeight);
                    break;
                case "T":
                    taxiCounter++;
                    if (tileMapData[i + 1][j] != "." || tileMapData[i][j + 1] != "." || tileMapData[i][j + 2] != "." ||
                        tileMapData[i + 1][j + 1] != "." || tileMapData[i + 1][j + 2] != ".")
                    {
                        parsingMessage += "<p>The Taxi would start in an impossible location.</p>";
                        parsingError = true;
                    }
                    break;
                default:
                    if (regDiver.test(tileMapData[i][j]))
                    {
                        if (tileMapData[i + 1][j] != "#")
                        {
                            parsingMessage += "<p>Diver(s) must be located directly on a platform.</p>";
                            parsingError = true;
                        }
                    }
                    ctx.fillStyle = "#000000";
                    ctx.fillRect((j * tileWidth), (i * tileHeight), tileWidth, tileHeight);
            }
        }
    }

    //Further error handling
    if (exitCounter < 3 || validExitFound == false || taxiCounter != 1)
    {
        parsingMessage += "<p>There are not enough exit symbols and/or no/too much taxi starting points</p>";
        parsingError = true;
    }

    /*
        ..create a small snapshot of canvas after the map loaded the first time.
        Because we do want to implement a feature such as a level-select-screen we do have to think about this: How is it possible to present the given maps to the player in a convenient way?
        The basic idea is to take a snapshot of the given canvas section, parse and/or save it as an image and use it as an icon within the level-select-screen later on.

        More information are available here:
        http://stackoverflow.com/questions/16792805/how-to-take-screenshot-of-canvas
        http://stackoverflow.com/questions/21227078/convert-base64-to-image-in-javascript-jquery
        http://www.w3schools.com/jsref/met_node_appendchild.asp
        Date of last access: 2016-08-02

        NOTE: self is being used to maintain a reference to the original THIS even as the context is changing.
                --> this.image.width: context is imageObject itself outside the function
                --> self.tWidth context refers to the attribute within the function
        You can get more information here: http://alistapart.com/article/getoutbindingsituations
    */

    /*
        Convert the drawn canvas into a valid dataStream of the type jpeg.
        The second parameter, a float, defines the quality of the resulting
        jpeg.
            1.0 - High quality
            0.5 - Medium quality
        ... You get the concept.
    */
    var dataURL = canvas.toDataURL("image/jpeg", 1.0);

    var image = new Image();
    image.src = dataURL;

    /*
        ResizeImage will shrink the image
        to a given width and calculate the height
        using the aspect ratio.

        @param imageObject      The image.
        @param targetWidth      The width to shrink to.
    */
    function ResizeImage(imageObject, targetWidth)
    {
        this.image = imageObject;
        this.tWidth = targetWidth;
        var ratio = 0;
        var self = this;
        ratio = Math.floor(this.image.width / self.tWidth);
        this.image.width = Math.floor(this.image.width / ratio);
        this.image.height = Math.floor(this.image.height / ratio);
    }

    //Only proceed if there isn't any error.
    if (!parsingError)
    {
        //Change the size of the Image()-Object with help of our function ResizeImage()
        ResizeImage(image, 300); //(image()-object, targetWidth)
        document.getElementById("thumbnail-viewer").innerHTML = ""; //Empty again
        document.getElementById("thumbnail-viewer").appendChild(image); //Better for UX
    }
    else
    {
        document.getElementById("thumbnail-viewer").innerHTML = "";
    }

    //Store all necessary data into a new object...
    var mapDataObject = {
        "map_name": fileName,
        "map_data": rawMapData,
        "thumb": dataURL
    };

    //And awaaaaay
    if (!parsingError)
    {
        uploadMap(mapDataObject);
    } //Back to MapUploader.js
    else
    {
        errorCon.innerHTML = parsingMessage;
        document.getElementById("files").disabled = false;
    } //Display error message
}
