/*
    We create an Array to store information about the different keys
    The eventListener further down will set the value of the fields to true
    if the button with the corresponding keyCode is being pressed
*/
var keys = [];

//Variables for movement
var xAccel = 0.10; //Variable used to store the x axis Acceleration
var yAccel = 0.075; //Variable used to store the y axis Acceleration
var minSpeed = 1.0;
var maxXSpeed = 2.5;
var maxYSpeed = 3.0;
var drag = 0.05;
var spaceKey = false;
var count = 0;
var gravity = 0;
/*
  This function is used to create movement.
  If any of the arrow keys are pressed the logical instance of the player will
  move towards the chosen direction with the defined speed.
*/
//Re-init for replay purposes.
function initPhysics()
{
    xAccel = 0.10;
    yAccel = 0.075;
    minSpeed = 1.0;
    maxXSpeed = 2.5;
    maxYSpeed = 3.0;
    drag = 0.05;
    spaceKey = false;
    count = 0;
    gravity = 0;
}

function movement()
{
    /*  KEY codes:
        up - 38
        down - 40
        left - 37
        right - 39
        space - 32
    */
    /*
        DEFINITION OF MOVEMENT:
        -1 SubMarineStatL   stationary, facing left       Frames 12-14      Element[0]
         1 SubMarineStatR   stationary, facing right      Frames 15-17      Element[1]
        -2 SubMarineMoveL   moving, facing left           Frames 0-5        Element[2]
         2 SubMarineMoveR   moving, facing right          Frames 6-11       Element[3]
        -3 SubMarineSinkL   sinking, facing left          Frames 18-20      Element[4]
         3 SubMarineSinkR   sinking, facing right         Frames 21-23      Element[5]
    */
    //Only allow steering when the player isn't sinking.
    if (!player.isSinking())
    {
        //Stop all divers when you are not colliding.
        for (var z = 0; z < NPO.divers.length; z++)
        {
            if (NPO.divers[z].shouldMove) NPO.divers[z].shouldMove = false;
        }
        // ========================
        // UP AND DOWN DIRECTION/MOVEMENT
        if (keys[38] && keys[40]) // BOTH BUTTONS PRESSED
        {

            if (player.ySpeed <= yAccel && player.ySpeed >= -yAccel)
            {
                player.ySpeed = 0.0; // Set the speed to 0 when getting close
                player.moveAnimation("none");
            }
            else
            {
                player.ySpeed *= 0.95;
                player.moveAnimation("none");
            }
            player.y += player.ySpeed;
        }
        else if (keys[38]) // UP MOVEMENT
        {
            player.moveAnimation("up");
            if (player.ySpeed > -maxYSpeed)
            {
                player.ySpeed -= yAccel;
            }
            player.y += player.ySpeed;
        }
        else if (keys[40]) // DOWN MOVEMENT
        {

            player.moveAnimation("down");
            if (player.ySpeed < maxYSpeed)
            {
                player.ySpeed += yAccel;
            }
            player.y += player.ySpeed;
        }
        // ================================
        // LEFT AND RIGHT MOVEMENT
        if (keys[37] && keys[39]) // BOTH BUTTONS PRESSED
        {
            if (player.xSpeed <= xAccel && player.xSpeed >= -xAccel)
            {
                player.xSpeed = 0.0; // To set the speed to 0 when getting close
                player.moveAnimation("none");
            }
            else
            {
                player.xSpeed *= 0.95;
                player.moveAnimation("none");
            }
            player.x += player.xSpeed;

        }
        else if (keys[37]) // LEFT MOVEMENT
        {
            player.moveAnimation("left"); //Left Heading Animation Sequence
            player.direction = -2;
            if (player.xSpeed > -maxXSpeed) // player moving slower than max?
            {
                player.xSpeed -= xAccel;
            }
            player.x += player.xSpeed;
        }
        else if (keys[39]) // RIGHT MOVEMENT
        {
            player.moveAnimation("right"); //Right Heading Animation Sequence
            player.direction = 2;
            if (player.xSpeed < maxXSpeed) // player moving slower than max?
            {
                player.xSpeed += xAccel;
            }
            player.x += player.xSpeed;
        }
        // END MOVEMENT
        // ================================
        // If none of the arrow keys is pressed
        if ((!keys[37]) && (!keys[38]) && (!keys[39]) && (!keys[40]) && (!keys[32]))
        {
            player.moveAnimation("none");
        }

        // =============================
        /*
            The drag moves the player in a bit into the direction
            he last steered to.
        */
        // UP AND DOWN DRAG
        if ((!keys[38]) && (!keys[40]))
        {
            // DOWN
            if (player.ySpeed > 0) // if player is moving down
            {
                if (player.ySpeed > 0 && player.ySpeed < yAccel)
                {
                    player.ySpeed = 0;
                }
                else
                {
                    player.ySpeed -= drag;
                }
                player.y += player.ySpeed;
            }
            // UP
            else if (player.ySpeed < 0) // if player is moving up
            {
                if (player.ySpeed < 0 && player.ySpeed > -yAccel)
                {
                    player.ySpeed = 0;
                }
                else
                {
                    player.ySpeed += drag;
                }
                player.y += player.ySpeed;
            }

        }

        // LEFT AND RIGHT DRAG
        if ((!keys[37]) && (!keys[39]))
        {
            // RIGHT
            if (player.xSpeed > 0) // if player is moving right
            {
                if (player.xSpeed > 0 && player.xSpeed < xAccel)
                {
                    player.xSpeed = 0;
                }
                else
                {
                    player.xSpeed -= drag;
                }
                player.x += player.xSpeed;
            }
            // LEFT
            else if (player.xSpeed < 0) // if player is moving left
            {
                if (player.xSpeed < 0 && player.xSpeed > -xAccel)
                {
                    player.xSpeed = 0;
                }
                else
                {
                    player.xSpeed += drag;
                }
                player.x += player.xSpeed;
            }

        }
    }
    else
    {
        if (player.xSpeed != 0)
        {
            //TODO implement smooth slowing down to simulate drag on momento
        }
    }
    // ================================
    // SINKING - flooding of air chambers and red alarm signals :)
    ///*
    if (spaceKey)
    { //spaceKey is being manipulated by the onkeyup-Event written on the init.js
        player.xSpeed = 0.0;
        player.ySpeed = 0.0;
        player.moveAnimation("sink");
        player.y += gravity;
    }
    else
    {
        gravity = 0.5;
    }
}
