/*
    Divers represent the passengers of the original game.
    Divers come with their specific position a lock tile displayed
    below them.

    @param animations   The array of diver movements.
    @param x            x-coordinate.
    @param y            y-coordinate.
    @param w            The diver width.
    @param h            The diver height.
    @param prio         The diver prio, set by the number in the map file.
    @param rowIndex     The diver row in the two-dimensional map representation.
    @param columnIndex  The diver column in the two-dimensional map representation.
*/
function Diver(animations, x, y, w, h, prio, rowIndex, columnIndex)
{
    this.animations = animations;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.prio = prio;
    this.blockIndex = -2;
    this.platform = -2;
    this.shouldMove = false;
    this.pickedUp = false;

    this.rowIndex = rowIndex;
    this.columnIndex = columnIndex;

    /*
        0 - Front
        1 - Left
        2 - Right
        3 - Back
    */
    this.direction = 0;

    /*
        Change the direction based on the input.

        @param trigger    The direction to change to.
    */
    this.moveAnimation = function(trigger)
    {
        switch (trigger)
        {
            case "front":
                this.direction = 0;
                break;
            case "left":
                this.direction = 1;
                break;
            case "right":
                this.direction = 2;
                break;
            case "back":
                this.direction = 3;
                break;
        }
    }

    //Return the diver priority.
    this.getPriority = function()
        {
            return this.prio;
        }
        //Draw only when the diver should move.
    this.draw = function()
        {
            if (this.shouldMove)
            {
                switch (this.direction)
                {
                    case 1:
                        this.x--;
                        break;
                    case 2:
                        this.x++;
                        break;
                }
            }
            if (!this.pickedUp) this.animations[this.direction].draw(this.x, this.y);
        }
        //Update as long as the diver hasn't been picked up.
    this.update = function()
        {
            if (!this.pickedUp) this.animations[this.direction].update();
        }
        //One function to update them all.
    this.render = function()
    {
        this.draw();
        this.update();
    }
}
