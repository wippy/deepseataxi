/*
    returnHighscores will reutrn a table
    full of highscores and display them.
*/
function returnHighscores()
{
    console.log("Preparing Highscore AJAX.");
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
        if (xhttp.readyState == 4 && xhttp.status == 200)
        {
            document.body.removeChild(document.getElementById("response-loader-holder")); //Remove loading animation
            document.getElementById("response").innerHTML = xhttp.responseText; //Display
        }
    };
    xhttp.open("POST", "./assets/php/HighscoreRequestHandler.php", true);
    /*
        Because of the broken $_POST var in PHP,
        it's easier to send JSON and decode it -
        It provides a structure.
    */
    xhttp.send(
        JSON.stringify(
        {
            "getHighscores": true
        })
    );
    console.log("Highscore AJAX sent.");
}
