/*
    Init.js adds the key listener to our canvas
    and handles the key input.
    It is also responsible for creating the LevelSelection.
*/
var tempStatus = 0;
var count = 0;
var countup = 0;
var actualKey = 0;
var spaceKey = false;
//Re-init for the replay support.
function initKeyParam()
{
    tempStatus = 0;
    count = 0;
    countup = 0;
    actualKey = 0;
    spaceKey = false;
}
var handleKeyDown = function(e)
{
    actualKey = e.keyCode ? e.keyCode : e.which; //Cross Browser safety (FireFox)
    count++;
    e.preventDefault(); //Prevent scrolling via keys
    keys[actualKey] = true;
}
var handleKeyUp = function(e)
    {
        actualKey = e.keyCode ? e.keyCode : e.which; //Cross Browser safety (FireFox)
        countup++;
        e.preventDefault(); //Prevent scrolling via keys
        //The space key has a special meaning.
        if (actualKey == 32)
        {
            player.y -= 1;
            spaceKey = !spaceKey;
            player.sinking = !player.sinking;
        }
        delete keys[actualKey]; //Set to undefined.
    }
    //Add the event listener.
document.getElementById("game").addEventListener("keydown", handleKeyDown, false);
document.getElementById("game").addEventListener("keyup", handleKeyUp, false);
//Init the LevelSelection.
new LevelSelection().init();
