/*
    The MapUploader serves the purpose to check the map file itself
    for validity and uploading it.
*/
var filesDiv;
var thumbView;
/*
    @param evt      The change event caused on the HTML file picker button.
*/
function validateFile(evt)
{
    console.log("Starting file type validation...")

    //Clean up
    thumbView = document.getElementById("thumbnail-viewer");
    thumbView.innerHTML = ""; //Remove old thumbnails from page
    filesDiv = document.getElementById("files");
    filesDiv.disabled = true; //Disable to prevent further clicking.
    //Get all files submitted through the picker. (1 allowed)
    var files = evt.target.files;

    //Only one file possible, therefore hardcoded access.
    var possibleMap = files[0];

    //Check the file extension.
    if (possibleMap.name.slice((Math.max(0, possibleMap.name.lastIndexOf(".")) || Infinity) + 1) == "map")
    {
        //Continue
        console.log("File type ok.");
        var reader = new FileReader(); //We have to read the file contents.
        reader.onload = function(e)
        {
            //Reading finished, submit to the map parser.
            parseMap(possibleMap.name, e.target.result);
        }

        //Start the file reader process.
        reader.readAsText(possibleMap);

    }
    else
    {
        document.getElementById("error-message").innerHTML = "<h2>Your map doesn't meet the required standards:</h2><p>The file is no .map file!</p>";
        console.log("No .map-file!");
    }
}
/*
    @param data   Contains the map data and the byte64 encoded thumb data.
*/
function uploadMap(data)
{
    var strData = JSON.stringify(data);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
        //Causes one known error. Don't worry.
        var response = JSON.parse(xhttp.response);

        if (xhttp.readyState == 4 && xhttp.status == 200)
        {
            (response.success > -1) ? console.log("Map upload complete and successful."): console.log("Map upload for map completed with errors and stopped processing.");
            //Re-enable form.
            filesDiv.disabled = false;
        }
    };
    xhttp.open("POST", "./assets/php/ProcessMapUpload.php", true);
    xhttp.send(strData);
}
