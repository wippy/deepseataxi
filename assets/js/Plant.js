/*
    A plant is part of the "Nippes 'n Stuff" to beautify the map.

    @param ctx          The canvas context. Not needed.
    @param animations   The plant Animation.
    @param x            x-coordinate.
    @param y            y-coordinate.
    @param w            The width.
    @param h            The height.
*/
function Plant(ctx, anim, x, y, w, h)
{
    this.ctx = ctx;
    this.anim = anim;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.damage = 0;

    this.render = function()
    {
        //this.anim.update();
        //Draw the plant.
        this.anim.draw(this.x, this.y);
    }
}
