/*
        As there is no direct way* to generate a random number generator with JS it is necessary to implement one on our own.
        *Math.random() do generate an number between 0 to 1.
        function random() will be our random number generator. The parameter "range" will stand for the possible output range, e.g. 400 -> possible output: 0-400!

        How it works:
            (1) Math.ceil(Math.random() * 1000) | generate an number between 0 and 1.. multiply it with 1000. Math.ceil() will round up to the next integer value.
            (2) ... % range + 1                 | % to stay within the allowed range. +1 cause moldulo opration delivers a result between 0 and range-1.

        Example:    range = 400;
                    Math.random = 0.73885386549 .. *1000 ..  Math.ceil -> 739  % 400 = 400 + 339(R) => 340
                    .
                    (?) moldulo opration delivers a result between 0 and range-1 ? --> Math.random = 0.39888888.. *1000 ..  Math.ceil -> 399  % 400 = 0 + 399(R) => 399+1 = 400 (max)
                    .
*/
function random(range)
{
    return Math.ceil(Math.random() * 1000) % range + 1;
}

console.log("randomGenerator randoms number: " + random(500));
