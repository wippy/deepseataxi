/*
    A normal entity which doesn't have to be that
    special to have its own file.

    @param ctx        The canvas context.
    @param img        The image object to draw.
    @param x          The x-coordinate.
    @param y          The y-coordinate.
    @param w          The width.
    @param h          The height.
    @param difficulty The global difficulty to determine the damage (if needed.).
*/
function Entity(ctx, img, x, y, w, h, difficulty)
{
    this.ctx = ctx;
    this.img = img;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.difficulty = difficulty;

    this.damage = (this.difficulty) ? 100 : 50;

    this.render = function()
    {
        this.ctx.drawImage(this.img,
            this.x,
            this.y,
            this.w,
            this.h);
    }
}
