//Create the level selection.
function LevelSelection()
{
    this.levelSelectionContainer = null;
    this.mapList;
    this.thumbnailImageList;
    this.mapDirectory = "./assets/maps/";
    this.placeholderThumb = "./assets/img/placeholderThumb.jpg";

    this.init = function()
        {
            this.levelSelectionContainer = document.getElementById("game-level-selection"); //Init container
            this.requestMapList();

        }
        //The AJAX call to retrieve a list of maps / thumbs.
    this.requestMapList = function()
    {
        var self = this; //We can't access the needed "this" in the listener below.
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function()
        {
            if (xhttp.readyState == 4 && xhttp.status == 200)
            {
                self.mapList = JSON.parse(xhttp.responseText);
                self.preloadThumbnails();
            }
        };

        xhttp.open("GET", "./assets/php/LevelSelectionMapRequest.php", true);
        xhttp.send();
    }
    this.preloadThumbnails = function()
        {
            var remainingThumbs = this.mapList.length;
            var currentThumb;
            var self = this;
            this.thumbnailImageList = [];

            for (var i = 0; i < remainingThumbs; i++)
            {
                currentThumb = new Image(); //Create a new Image object for every thumb.

                currentThumb.onload = function()
                {
                    --remainingThumbs;

                    //Check if there are thumbs left.
                    if (remainingThumbs <= 0)
                    {
                        console.log("Preload of level selection done.");

                        //CAAAAALLBACK - This time without submitting it to the function.
                        self.renderSelection();
                    }

                };
                //Display a default thumb if the map has no thumbnail.
                currentThumb.src = (this.mapList[i].thumb.indexOf("none") > -1) ? this.placeholderThumb : this.mapDirectory + "" + this.mapList[i].thumb;
                this.thumbnailImageList.push(currentThumb);
            }
        }
        //Display the result in its container.
    this.renderSelection = function()
    {
        var result = "";
        var groupSet = false;
        var currentGroup = 0;
        for (var i = 0; i < this.thumbnailImageList.length; i++)
        {
            //Determine if the map is from the same group.
            var tG = parseInt(this.mapList[i].map.split("level")[1].charAt(0));
            if (tG !== currentGroup) groupSet = false; //A new group appeared.
            if (!groupSet)
            { //Not the same group, set the next one.
                currentGroup = tG;
                groupSet = true;
                result += '<h1 class="under-water-small">Group ' + currentGroup + '</h1>';
            }
            result += '<a href="#" class="hovertext" title="' + this.mapList[i].map.split(".map")[0] + '" onClick="selectedMap=\'' + this.mapList[i].map + '\';DST.init();"><img src="' + this.thumbnailImageList[i].src + '" /></a>';
            if ((i + 1) % 2 == 0)
            {
                result += "<br>";
            } //Create a break after every other map thumb.
        }
        document.getElementById("response-loader-holder").style.display = "none";
        this.levelSelectionContainer.innerHTML = result;
    }
}
