/*
    Statistics is responsible for managing and displaying
    all status information the player needs.

    @param difficulty    The global difficulty to determine the time.
*/
function Statistics(difficulty)
{
    this.score = 0;
    this.lifesCon = null;
    this.scoreCon = null;
    this.score = 0;

    this.timerCon = null;
    this.timerInt = null;
    this.level = "";
    this.difficulty = difficulty;

    this.timerVal = (this.difficulty) ? 150 : 300;

    /* ===== Main ===== */
    this.init = function()
    {
        this.timerCon = document.getElementById("game-bar-timer-timer");
        this.timerCon.innerHTML = this.timerVal; //Initialize with start value

        this.lifesCon = document.getElementById("game-bar-lifes-lifes");
        this.lifesCon.innerHTML = player.health;

        this.scoreCon = document.getElementById("game-bar-score-score");
        this.scoreCon.innerHTML = this.score;

    }
    this.update = function()
    {
        this.lifesCon.innerHTML = Math.floor(player.health); //We wan't integers.
        this.scoreCon.innerHTML = Math.floor(this.score);
    }

    /* ===== Timer ===== */
    this.startTimer = function()
    {
        var self = this; //Needed because we can't access the actual "this" in the anonymous function.
        this.timerInt = setInterval(function()
        {
            self.timer();
        }, 1000);
        console.log("Timer started.");
    }
    this.stopTimer = function()
    {
        clearInterval(this.timerInt);
        console.log("Timer stopped");
    }
    this.timer = function()
    {
        this.timerVal--;
        if (this.timerVal <= 0)
        { //Time's up, end the game and kill the player.
            this.timerCon.innerHTML = "00" + (this.timerVal < 0) ? this.timerVal = 0 : this.timerVal; //One last time to get the 0
            this.scoreCon.innerHTML = Math.floor(this.score);
            clearInterval(this.timerInt);
            player.kill();
            return;
        }

        //Add leading 0
        if (this.timerVal < 100 && this.timerVal >= 10) this.timerVal = "0" + this.timerVal;
        if (this.timerVal < 10) this.timerVal = "00" + this.timerVal;
        this.timerCon.innerHTML = this.timerVal;
    }
    this.getTimeLeft = function()
    {
        return this.timerVal;
    }
    this.setTimer = function(t)
    {
        this.timerVal = t;
    }

    /* ===== Score ===== */
    this.setScore = function(_score)
    {
        this.score += _score;
    }
    this.getScore = function()
    {
        return this.score;
    }

    /* ===== Level ===== */
    this.setLevel = function(_level)
    {
        this.level = _level;
    }
    this.getLevel = function()
    {
        return this.level;
    }
}
