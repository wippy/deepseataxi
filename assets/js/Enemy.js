/*
    All Octupus on the map are instances of Enemey.

    @param animations   The array of diver movements.
    @param audio        Contains the sound files.
    @param x            x-coordinate.
    @param y            y-coordinate.
    @param w            The diver width.
    @param h            The diver height.
    @param borderLX     The x value on the left side to stop at.
    @param borderRX     The x value on the right side to stop at.
    @param difficulty   The global difficulty value to determine speed and damage.
*/
function Enemy(animations, audio, x, y, w, h, borderLX, borderRX, difficulty)
{
    this.animations = animations;
    this.shouldMove = true;
    this.direction = (random(2) - 1); //Choose a random direction.

    //SOUND FX
    this._audio = audio;
    this.audio = document.createElement("audio");
    this.audio.src = this._audio.src;

    this.isAudioPlaying = false;

    //Reference point upper left corner
    this.x = x;
    this.y = y;
    //Width and height
    this.w = w;
    this.h = h;

    this.borderLX = borderLX;
    this.borderRX = borderRX;
    this.difficulty = difficulty;

    this.speed = (this.difficulty) ? 1 : 0.5;
    this.damage = (this.difficulty) ? (0.3 * 2) : 0.3;

    //Draw only if enemy should move.
    this.draw = function()
        {
            if (this.shouldMove)
            {
                switch (this.direction)
                {
                    case 0:
                        this.x += this.speed;
                        break;
                    case 1:
                        this.x -= this.speed;
                        break;
                }
            }

            this.animations[this.direction].draw(this.x, this.y);
        }
        //Update the enemy as long as it shouldn't stop at its x values.
    this.update = function()
        {
            if ((this.x + this.w) >= this.borderRX) this.direction = 1;
            if (this.x <= this.borderLX) this.direction = 0;

            this.animations[this.direction].update();
        }
        //One function to update them all.
    this.render = function()
    {
        this.draw();
        this.update();
    }

}
