/*
World.js is responsible for downloading the map from the server
and drawing / updating it.
*/
//Setting necessary vars.
var rows = 0; //Grid height	(Rows)
var cols = 0; //Grid width 	(Columns)
var grid = []; //Grid itself
var levelData = []; //Base levelData array
var chosenBackgroundImage = 0;
var chosenTrashImage = 0;

var nippesCoordinateStorage = []; //keeps the coordinates for nippes'n'stuff
var locationCounter = 0; //keep track of the locations (coordinates) found
var considerMaxLocations = 0; //how many possible positions should be considered as Nippes-Placements (uses random()!)

var c;
var ctx;

//Store map dimensions
var caW = 800; //c.width; //MAKE AMERICA DYNAMIC AGAIN
var caH = 600; //c.height;
var tileWidth = 25; //Tile width
var tileHeight = 25; //Tile height

//Maps directory path relative to play.html
var levelPath = "./assets/maps/";

//For diver <-> platform combinations
var platformGroup = 0;
var platformLastRow = 0;
var lastDiverIndex = 0;
var nextPlatformSameGroup = 0;

//Re-init for replay purposes
function initWorldParam()
{
    rows = 0; //Grid height	(Rows)
    cols = 0; //Grid width 	(Columns)
    grid = []; //Grid itself
    levelData = []; //Base levelData array
    chosenBackgroundImage = 0;
    chosenTrashImage = 0;

    nippesCoordinateStorage = []; //keeps the coordinates for nippes'n'stuff
    locationCounter = 0; //keep track of the locations (coordinates) found
    considerMaxLocations = 0; //how many possible positions should be considered as Nippes-Placements (uses random()!)

    platformGroup = 0;
    platformLastRow = 0;
    lastDiverIndex = 0;
    nextPlatformSameGroup = 0;
}
/*
    loadLevel downloads the level from the server and does basic
    format work.

    @param fileName		The level file to load.
*/
function loadLevel(fileName)
{
    //Set only the level number (e.g. 201) as level for the highscore table.
    statistics.setLevel(fileName.split("level")[1].split(".map")[0]);

    //New AJAX request.
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
        if (xhttp.readyState == 4 && xhttp.status == 200)
        {
            var lvl = xhttp.responseText.replace(/\s+/g, "").split(""); //Remove whitespaces & line breaks& split into single characters
            var k = 0; //Counter for split array (lvl) position
            /*
                Determine rows:
                Split responseText at line breaks "\n" and count the resulting array length.
            */
            rows = xhttp.responseText.split("\n").length;

            //Take the already known map length (Standard: 768) and divide through rows to get the cols.
            cols = ((lvl.length) / rows);

            for (var i = 0; i < rows; i++)
            {
                levelData[i] = []; //Initialize 2nd dimension

                for (var j = 0; j < cols; j++)
                {
                    levelData[i][j] = lvl[k];
                    k++;
                }
            }
            /*
                .. find all coordinates within the level which are suitable for "nibbes / stuff"

                The following variables are created globally:
                var nippesCoordinateStorage = []; 	//keeps the coordinates for nippes'n'stuff
                var locationCounter = 0;			//keep track of the locations (coordinates) found

                First of all our code has to find all suitable locations (coordinates) for "nippes and stuff" within our level.
                Therefore we step into the grid at position [2][1] and move through it position by position. While we are doing this we check
                if the position is okay or not. But how?

                We do not want to place some stuff somewhere into free water(space). So there has to be at least ground, which is a platform or the bottom of the map.
                These abovementioned locations are defined as either "#" (platform) or "R" (ger.: Rand <-> eng.: surrounding/bottom).

                Next step is to make sure that the space above platform/bottom is free and/or sutiable. Because i do use sprites with a height of 50px the next two grid sections
                above a suitable location must be free. Right now free/suitable is defined by either "." (free space/water).

                All coordination will be saved like: [8 4] whereas [] shall be an optical sign for the array. First number stands for the x-coordinate; second for the y-coordinate.
                Space " " is being used for separation.

                !Note: The defintion of free/suitable can change through time as it depends on the sprites and their properties we want to use.

            */
            for (var i = 1; i < rows; ++i)
            { //i is the x-coordinate
                for (var j = 1; j < cols; j++)
                { //j is the y-coordinate
                    if (levelData[i][j] == "#" || levelData[i][j] == "R")
                    { //find bottom or platform
                        if (levelData[i - 1][j] == "." && levelData[i - 2][j] == ".")
                        { //next two grid sections above suitable?
                            nippesCoordinateStorage[locationCounter] = ((i - 2) + " " + j); //save coordinates of 2 grid sections above suitable location <-> 50px sprite in use!
                            locationCounter += 1; //Suitable location has been found! keep track..

                            //console.log("Found location at [i] | [j] " +(i-1) +" | " +j);
                        }
                    }
                }
            }
            /*

                .. At this point we found a maximum suitable locations of: locationCounter. We saved the coordinates into array: nippesCoordinateStorage.
                .. Now make following decisions on your(->code) own:
                (1) How many of the half-maximum allowed locations (all is to much for such a small map) do you want to consider? | randomGenerator in use! ;-)
                (2) For the given decision (1) we would still have the posibility of too much stuff. So decide again with the helf of Math.random()
                (3) If the coincidence wants to place something at the position there is some work to do:
                3.1	Decide on your own(-> code) which of all suitable location you want to use. Split the string withing the array into x- & y- coorinate.
                3.2 Replace the default symbol with a special symbol "A" at the given position.

            */
            considerMaxLocations = random(Math.ceil((locationCounter * 2) / 3)); //see above: (1)
            for (i = 0; i < considerMaxLocations; i++)
            {
                var temp = Math.random();
                if (temp >= 0.2 && temp <= 0.8)
                { //see above (2)
                    var coord = nippesCoordinateStorage[random(locationCounter - 1)].split(" "); //see above (3.1)
                    levelData[coord[0]][coord[1]] = "A"; //see above (3.2) as well as the following switch-case made by Author: PR
                }
                else if (temp < 0.18)
                { //see above (2)
                    var coord = nippesCoordinateStorage[random(locationCounter - 1)].split(" "); //see above (3.1)
                    levelData[coord[0]][coord[1]] = "B"; //see above (3.2) as well as the following switch-case made by Author: PR
                }
            }

            initMapBuilder(levelData);
        }
    };

    xhttp.open("GET", levelPath + fileName, true);
    xhttp.send();
}

/*
    initMapBuilder gets called after the script
    retrieved the map file.

    @param g   The two-dimensonal map data
*/
function initMapBuilder(g)
{
    c = document.getElementById("game");
    ctx = c.getContext("2d");
    var ran = Math.floor(Math.random() * (gameAssets.backgrounds.length - 1)); //Get a random number based on the amount of backgrounds we have
    chosenBackgroundImage = gameAssets.backgrounds[ran]; //Get a background image based on the random number we generated.
    drawMap(ctx, g);
}
/*
    drawMap will parse and draw the map once.
    This function is responsible for filling NPO
    with its objects.

    @param ctx      The canvas context.
    @param g        The two-dimensonal map data.
*/
function drawMap(ctx, g)
{
    /*
        Before drawing again,
        we will clear the canvas once,
        so that we won't layer up our
        objects.
    */
    ctx.clearRect(0, 0, caW, caH);

    //Calculate the tile dimensions based on the canvas and map dimensions. (800x600 and 32x24 should  get us 25x25)
    tileWidth = caW / cols;
    tileHeight = caH / rows;

    //Regular expression to determine a number (decimal) in a string.
    var regDiver = /\d+$/;

    //Draw the background as the first object.
    ctx.drawImage(chosenBackgroundImage, 0, 0, caW, caH);

    //Iterate through the two dimensional map data. (Row-wise)
    for (var i = 0; i < rows; i++)
    {
        for (var j = 0; j < cols; j++)
        {
            //The handling using a switch is more efficient than using if's.
            switch (g[i][j])
            {
                case "R":
                    var boundary = new Entity(ctx, gameAssets.sprites.edge, (j * tileWidth), (i * tileHeight), 25, 25);
                    NPO.boundaries.push(boundary);
                    boundary.render();
                    break;
                case ".":
                    //Sky
                    //Do nothing. Sky is beautiful. Stare at Sky.
                    break;
                    //SPECIAL character "A"
                    //used to position the abovementioned "nippes" stuff.
                case "A":
                    var tempDecision = (random(400) % 2 == 0) ? SeaWeedBigMove : PlantMove;
                    var posCorrection = 0;
                    if (tempDecision == SeaWeedBigMove)
                    {
                        posCorrection = 5;
                    }
                    else if (tempDecision == PlantMove)
                    {
                        posCorrection = 11;
                    }
                    var plant = new Plant(ctx, tempDecision, (j * tileWidth - posCorrection), (i * tileHeight + posCorrection), 25, 50);
                    NPO.plants.push(plant);
                    plant.render();
                    break;
                case "B":
                    //B means trash.
                    var trash = new Entity(ctx, gameAssets.trash[random(gameAssets.trash.length - 1)], (j * tileWidth), ((i + 1) * tileHeight), 25, 25);
                    NPO.entities.push(trash);
                    trash.render();
                    break;
                case "#":
                    //--Check platform tile orientation
                    //Single tile
                    if (g[i][j - 1] == "." && g[i][j + 1] != ".")
                    {
                        if (g[i - 1][j] == "#")
                            var platform = new Platform(ctx, gameAssets.sprites.castleCenter, (j * tileWidth), (i * tileHeight), 25, 25); //Tile above
                        else
                            var platform = new Platform(ctx, gameAssets.sprites.castleLeft, (j * tileWidth), (i * tileHeight), 25, 25); //Castle left
                    }
                    else if (g[i][j + 1] == "." && g[i][j - 1] != ".")
                    {
                        if (g[i - 1][j] == "#")
                            var platform = new Platform(ctx, gameAssets.sprites.castleCenter, (j * tileWidth), (i * tileHeight), 25, 25); //Tile above
                        else
                            var platform = new Platform(ctx, gameAssets.sprites.castleRight, (j * tileWidth), (i * tileHeight), 25, 25); //Castle right
                    }
                    else if (g[i][j + 1] == "." && g[i][j - 1] == ".")
                    {
                        if (g[i - 1][j] == "#")
                            var platform = new Platform(ctx, gameAssets.sprites.castleCenter, (j * tileWidth), (i * tileHeight), 25, 25);
                        else
                            var platform = new Platform(ctx, gameAssets.sprites.castleAlone, (j * tileWidth), (i * tileHeight), 25, 25);
                    }
                    else
                    {
                        if (g[i - 1][j] == "#")
                            var platform = new Platform(ctx, gameAssets.sprites.castleCenter, (j * tileWidth), (i * tileHeight), 25, 25);
                        else
                            var platform = new Platform(ctx, gameAssets.sprites.castleMid, (j * tileWidth), (i * tileHeight), 25, 25);
                    }

                    //Replace castle tiles underneath divers with locks.
                    if (regDiver.test(g[i - 1][j]))
                    {
                        switch (g[i - 1][j])
                        {
                            case "1":
                                platform.img = gameAssets.sprites.box1Blue;
                                break;
                            case "2":
                                platform.img = gameAssets.sprites.boxLockedYellow;
                                break;
                            case "3":
                                platform.img = gameAssets.sprites.boxLockedRed;
                                break;
                            case "4":
                                platform.img = gameAssets.sprites.boxLockedGreen;
                                break;
                            case "5":
                                platform.img = gameAssets.sprites.boxLockedBlack;
                                break;
                            case "6":
                                platform.img = gameAssets.sprites.boxLockedPurple;
                                break;
                        }
                    }

                    //Error handling: Check if tile above valid
                    if (g[i - 1][j] != "R" && g[i - 1][j] != "#")
                    {
                        //If last tile platform -> same Group
                        if (nextPlatformSameGroup)
                        {
                            platform.group = platformGroup;
                        }
                        else
                        {
                            platformGroup++;
                            platform.group = platformGroup;
                        }
                    }
                    else
                    {
                        platformGroup++;
                        platform.group = -1;
                    }
                    //If next tile platform -> Same group
                    nextPlatformSameGroup = (g[i][j + 1] == "#") ? true : false;

                    //Get diver above platform
                    if (regDiver.test(g[i - 1][j]))
                    {

                        for (var z = 0; z < NPO.divers.length; z++)
                        {
                            //Check if found diver is the one we need.
                            if ((NPO.divers[z].getPriority() === parseInt(g[i - 1][j])) && (NPO.divers[z].rowIndex === (i - 1) && NPO.divers[z].columnIndex === j))
                            {
                                NPO.divers[z].platform = platformGroup;
                                NPO.divers[z].blockIndex = NPO.platforms.length;
                            }
                        }
                    }

                    //Only do one push and draw :)
                    NPO.platforms.push(platform);
                    platform.render();

                    break;
                case "E":
                    //Map entry / exit - If needed.
                    /*
                    Somewhere in here is a bug.
                    I commited the bug to the issue tracker.

                    Using exits (group()) as the container for the exit tiles,
                    the tiles will stretch out to the end of the map.
                    When we use the platform container (test-wise), it will dislpay
                    them correctly but give them physics (We don't want that).
                    */
                    var exit = new Exit(ctx, [gameAssets.sprites.exitClosed,
                        gameAssets.sprites.exitBlank,
                        gameAssets.sprites.arrowUp,
                        gameAssets.sprites.arrowDown,
                        gameAssets.sprites.arrowLeft,
                        gameAssets.sprites.arrowRight
                    ], (j * tileWidth), (i * tileHeight), 25, 25, i, j);
                    NPO.exit.push(exit);
                    exit.render();
                    break;
                case "X":
                    if (g[i + 1][j] != "." || (g[i + 1][j] == "." && g[i + 2][j] == "."))
                    {
                        var mine = new Entity(ctx, gameAssets.sprites.mineNoChain, (j * tileWidth), (i * tileHeight), 25, 25, globalDifficulty);
                    }
                    else
                    {
                        var mine = new Entity(ctx, gameAssets.sprites.mineWithChainSmall, (j * tileWidth), (i * tileHeight), 25, 50, globalDifficulty);
                    }
                    NPO.mines.push(mine);
                    NPO.explosions.push(new Explosion(new Animation(ExplodingMineSpr, 5, 0, 11), gameAssets.sounds.underwaterMineExplosion, (j * tileWidth - (tileWidth / 2)), (i * tileHeight - (tileHeight / 2)), 50, 50));
                    mine.render();
                    break;
                case "S":
                    var sign = new Entity(ctx, gameAssets.sprites.signExit, (j * tileWidth), (i * tileHeight), 25, 25);
                    NPO.entities.push(sign);
                    sign.render();
                    break;
                case "T":
                    /*
                    Currently, the Player creation happens in DeepSeaTaxi.js.
                    We re-set the x and y values here to their specific points.
                    */
                    player.x = (j * tileWidth);
                    player.y = (i * tileHeight);

                    player.explosion.x = (j * tileWidth);
                    player.explosion.y = (i * tileHeight);
                    break;
                case "Y":
                    var tempLinks = 0,
                        borderLX = 0;
                    var tempRechts = 0,
                        borderRX = 0;

                    //The enemies should move left and right until hitting a platform or boundary.
                    //The following loops search for these blocks and set the left and right x values.
                    for (var temp = j; temp >= 0; --temp)
                    {
                        //do something
                        if (g[i][temp] == "#" || g[i][temp] == "R")
                        {
                            tempLinks = g[i][temp];
                            borderLX = (temp * tileWidth) + tileWidth;
                            break;
                        }
                    }
                    for (var temp2 = j; temp2 <= 31; ++temp2)
                    {
                        //do something
                        if (g[i][temp2] == "#" || g[i][temp2] == "R")
                        {
                            tempRechts = g[i][temp2];
                            borderRX = (temp2 * tileWidth);
                            break;
                        }
                    }
                    NPO.enemies.push(new Enemy([new Animation(OctopussySpr, 6, 29, 39),
                            new Animation(OctopussySpr, 6, 8, 16)
                        ], gameAssets.sounds.OctopussyPain, (j * tileWidth), (i * tileHeight), 35, 38, borderLX, borderRX, globalDifficulty)

                    );
                    break;
                default:
                    //Test if current tile is a diver (decimal number)
                    if (regDiver.test(g[i][j]))
                    {
                        /*
                        Animation order:

                        Blue:
                        - Front (0-2)
                        - Left	(12-14)
                        - Right	(24-26)
                        - Back	(36-38)

                        Yellow:
                        - Front	(9-11)
                        - Left	(21-23)
                        - Right	(33-35)
                        - Back	(45-47)

                        Red:
                        - Front (3-5)
                        - Left 	(15-17)
                        - Right	(27-29)
                        - Back	(39-41)

                        Green:
                        - Front	(6-8)
                        - Left	(18-20)
                        - Right	(30-32)
                        - Back	(42-44)

                        Black:
                        - Front	(57-59)
                        - Left	(69-71)
                        - Right	(81-83)
                        - Back	(93-95)

                        Purple:
                        - Front	(51-53)
                        - Left	(63-65)
                        - Right	(75-77)
                        - Back	(87-89)

                        blue:	1
                        yellow:	2
                        red:	3
                        green:	4
                        black:	5
                        purple: 6
                        */
                        var frontSprStart = 0,
                            frontSprEnd = 2;
                        var leftSprStart = 12,
                            leftSprEnd = 14;
                        var rightSprStart = 24,
                            rightSprEnd = 26;
                        var backSprStart = 36,
                            backSprEnd = 38;

                        /*
                        We support up to 6 different divers.
                        Starting from the seventh diver will just
                        receive a blue suit.
                        */
                        switch (parseInt(g[i][j]))
                        {
                            case 1: //BLUE
                                frontSprStart = 0, frontSprEnd = 2;
                                leftSprStart = 12, leftSprEnd = 14;
                                rightSprStart = 24, rightSprEnd = 26;
                                backSprStart = 36, backSprEnd = 38;
                                break;
                            case 2: //YELLOW
                                frontSprStart = 9, frontSprEnd = 11;
                                leftSprStart = 21, leftSprEnd = 23;
                                rightSprStart = 33, rightSprEnd = 35;
                                backSprStart = 45, backSprEnd = 47;
                                break;
                            case 3: //RED
                                frontSprStart = 3, frontSprEnd = 5;
                                leftSprStart = 15, leftSprEnd = 17;
                                rightSprStart = 27, rightSprEnd = 29;
                                backSprStart = 39, backSprEnd = 41;
                                break;
                            case 4: //GREEN
                                frontSprStart = 6, frontSprEnd = 8;
                                leftSprStart = 18, leftSprEnd = 20;
                                rightSprStart = 30, rightSprEnd = 32;
                                backSprStart = 42, backSprEnd = 44;
                                break;
                            case 5: //BLACK
                                frontSprStart = 57, frontSprEnd = 59;
                                leftSprStart = 69, leftSprEnd = 71;
                                rightSprStart = 81, rightSprEnd = 83;
                                backSprStart = 93, backSprEnd = 95;
                                break;
                            case 6: //PURPLE
                                frontSprStart = 51, frontSprEnd = 53;
                                leftSprStart = 63, leftSprEnd = 65;
                                rightSprStart = 75, rightSprEnd = 77;
                                backSprStart = 87, backSprEnd = 89;
                                break;
                            default:
                                break;
                        }
                        //Only one single push
                        NPO.divers.push(new Diver([
                            new Animation(DiversSpr, 6, frontSprStart, frontSprEnd), //Front
                            new Animation(DiversSpr, 6, leftSprStart, leftSprEnd), //Left
                            new Animation(DiversSpr, 6, rightSprStart, rightSprEnd), //Right
                            new Animation(DiversSpr, 6, backSprStart, backSprEnd) //Back
                        ], (j * tileWidth), (i * tileHeight), 25, 25, parseInt(g[i][j]), i, j));
                    }
            }
        }
        //Increase after each row.
        platformGroup++;
    }
    //This will avoid a weird behavior when the diver 1 is missing.
    checkRemainingDivers();

    DST.start();
}
//Update all objects.
function updateMap()
{
    ctx.clearRect(0, 0, caW, caH); //Clear first to avoid the solitaire winning effect
    ctx.drawImage(chosenBackgroundImage, 0, 0, caW, caH);


    for (var i = 0; i < NPO.boundaries.length; i++)
    {
        NPO.boundaries[i].render();
    }
    for (var i = 0; i < NPO.platforms.length; i++)
    {
        NPO.platforms[i].render();
    }
    for (var i = 0; i < NPO.plants.length; i++)
    {
        NPO.plants[i].render();
    }
    for (var i = 0; i < NPO.mines.length; i++)
    {
        NPO.mines[i].render();
    }
    for (var i = 0; i < NPO.entities.length; i++)
    {
        NPO.entities[i].render();
    }
    for (var i = 0; i < NPO.enemies.length; i++)
    {
        NPO.enemies[i].render();
    }
    for (var i = 0; i < NPO.exit.length; i++)
    {
        NPO.exit[i].render();
    }

    for (var i = 0; i < NPO.explosions.length; i++)
    {
        NPO.explosions[i].render();

        if (NPO.explosions[i].getAnimation().hasEnded())
        {
            NPO.explosions[i].shouldExplode = false;
            NPO.explosions.splice(i, 1);
        }
    }
    //DIVER
    for (var i = 0; i < NPO.divers.length; i++)
    {
        NPO.divers[i].render();
    }

    if (player.isDead())
    {
        player.explode();
        //Draw skull
        ctx.drawImage(gameAssets.bling[0], (caW / 2) - ((gameAssets.bling[0].width * 3) / 2), (caH / 2) - ((gameAssets.bling[0].height * 3) / 2), (gameAssets.bling[0].width * 3), (gameAssets.bling[0].height * 3));

    }
}
