/*
This function detects collision between the player and all NPOs (Non playable object) aka all objects you can collide with.
When collision is detected the players movement towards the object he is colliding with are stopped.
*/
var nextDiverPrio = 1; //Which divers have to be collected next
var foundDiverWithSamePrioAsBefore = false;
var foundDiverWithRecentlyIncreasedPrio = false;

//Re-init the vars for replay
function initCollisionParam()
{
    nextDiverPrio = 1;
    foundDiverWithSamePrioAsBefore = false;
    foundDiverWithRecentlyIncreasedPrio = false;
}
/*
Check for remaining drivers with the same number as before,
or increase the the priority to collect next.
*/
function checkRemainingDivers()
{
    foundDiverWithSamePrioAsBefore = false;
    foundDiverWithRecentlyIncreasedPrio = false;
    //Check here for another driver with the same number to support the lock change below
    for (var z = 0; z < NPO.divers.length; z++)
    {
        if (NPO.divers[z].getPriority() == nextDiverPrio)
        {
            foundDiverWithSamePrioAsBefore = true;
        }
    }
    /*
    Only check for increase if there is a diver left and there isn't a diver with the same number as you have collected before.
    */
    if (!foundDiverWithSamePrioAsBefore && NPO.divers.length > 0)
    {
        nextDiverPrio++; //Increase....
        while (!foundDiverWithRecentlyIncreasedPrio) //Running until Diver with the next Prio is found
        {
            for (var z = 0; z < NPO.divers.length; z++)
            {
                if (NPO.divers[z].getPriority() == nextDiverPrio)
                {
                    foundDiverWithRecentlyIncreasedPrio = true;
                }
            }
            if (!foundDiverWithRecentlyIncreasedPrio)
            {
                nextDiverPrio++; //If none was found up nextDiverPrio by one
                console.log("Increased again to " + nextDiverPrio + " with " + NPO.divers.length + " divers left.");
            }
        }
    }
    /* Change the tile below the diver. */
    for (var x = 0; x < NPO.divers.length; x++)
    {
        if (NPO.divers[x].getPriority() == nextDiverPrio)
        {
            switch (NPO.divers[x].getPriority())
            {
                case 1:
                    NPO.platforms[NPO.divers[x].blockIndex].img = gameAssets.sprites.box1Blue;
                    break;
                case 2:
                    NPO.platforms[NPO.divers[x].blockIndex].img = gameAssets.sprites.box2Yellow;
                    break;
                case 3:
                    NPO.platforms[NPO.divers[x].blockIndex].img = gameAssets.sprites.box3Red;
                    break;
                case 4:
                    NPO.platforms[NPO.divers[x].blockIndex].img = gameAssets.sprites.box4Green;
                    break;
                case 5:
                    NPO.platforms[NPO.divers[x].blockIndex].img = gameAssets.sprites.box5Black;
                    break;
                case 6:
                    NPO.platforms[NPO.divers[x].blockIndex].img = gameAssets.sprites.box6Purple;
                    break;
                default:
                    NPO.platforms[NPO.divers[x].blockIndex].img = gameAssets.sprites.box1Blue;
            }
        }
    }
}

function collision()
{
    var i = 0;
    var specialCollision = false;
    var platformHit = false;
    foundDiverWithSamePrioAsBefore = false;
    foundDiverWithRecentlyIncreasedPrio = false;

    for (var key in NPO)
    {
        //Check if NPO has the given key and exclude some object types (plants, normal entities, explosions)
        if (NPO.hasOwnProperty(key) && (i !== 3 && i !== 5 && i !== 6))
        {
            for (var j = 0; j < NPO[key].length; j++)
            {
                var obj = NPO[key][j]; //Store the current object

                if (player.x < obj.x + obj.w &&
                    player.x + player.w > obj.x &&
                    player.y < obj.y + obj.h &&
                    player.h + player.y > obj.y)
                {

                    //Platform collision when sinking
                    if (1 === i && player.isSinking())
                    {
                        specialCollision = true;
                        gravity = 0;
                        player.hasLanded = true;
                        platformHit = true;

                        for (var z = 0; z < NPO.divers.length; z++)
                        {
                            if (obj.group === NPO.divers[z].platform)
                            {
                                if (NPO.divers[z].getPriority() == nextDiverPrio)
                                {
                                    //Enable THE WALK of the diver on the specific platform.
                                    NPO.divers[z].shouldMove = true;
                                    //Now, detect where submarine is (relative to the diver)
                                    (NPO.divers[z].x <= player.x) ? NPO.divers[z].moveAnimation("right"): NPO.divers[z].moveAnimation("left");
                                }
                            }
                        }
                    }
                    //Normal collision (e.g. boundary) with something while sinking.
                    if (player.isSinking())
                    {
                        specialCollision = true;
                        gravity = 0; //Just collided with something.
                        player.hasLanded = true;
                    }
                    //Mines
                    if (4 === i)
                    {
                        specialCollision = true;
                        NPO[key].splice(j, 1); //Remove the mine forever

                        //Explode
                        NPO.explosions[j].shouldExplode = true;

                        gravity = 0.5;
                        player.health -= obj.damage;
                    }
                    //Enemy
                    if (2 === i)
                    {
                        specialCollision = true;
                        player.health -= obj.damage; //Just substract damage value
                    }
                    //Divers
                    /*
                    This servers two purposes: If colliding while sinking WITHOUT touching the platform, it will slow you down.
                    Otherwise, while hitting the platform, the diver will WALK.
                    */
                    if (7 === i && (obj.shouldMove || !platformHit))
                    {
                        specialCollision = true;
                        gravity = 0.3;
                        if (obj.shouldMove)
                        {
                            console.log("TAXI TAXI");
                            obj.shouldMove = false;
                            obj.pickedUp = true; //Will stop drawing

                            statistics.setScore(50); //Doesn't work as expected

                            //Remove the diver.
                            NPO[key].splice(j, 1);

                            checkRemainingDivers(); //Check for remaining divers again (and their numbers)
                        }
                    }
                    // Exits
                    if (i == 8 && NPO.divers.length <= 0)
                    {
                        specialCollision = true;
                        var tempPlayerRight = player.x + player.w;
                        var tempPlayerBottom = player.y + player.h;

                        /*
                        Below:
                        Detect which on which map side the player collides with the exit.
                        */
                        //Leaving Map via the LEFT
                        if (4 == obj.exitType)
                        {
                            player.x -= 0.25
                            if (tempPlayerRight <= 2.0)
                            {
                                gameWon = true;
                            }
                        }
                        //Leaving Map via the RIGHT
                        if (5 == obj.exitType)
                        {
                            player.x += 0.3;
                            if (player.x >= 798.0)
                            {
                                gameWon = true;
                            }
                        }
                        //Leaving Map via the TOP
                        if (2 == obj.exitType)
                        {
                            player.y -= 0.3;
                            if (tempPlayerBottom <= 2.0)
                            {
                                gameWon = true;
                            }
                        }
                        //Leaving Map via the BOTTOM
                        if (3 == obj.exitType)
                        {
                            player.y += 0.3;
                            if (player.y >= 598.0)
                            {
                                gameWon = true;
                            }
                        }
                    }
                    /*  KEY codes:
                    up - 38
                    down - 40
                    left - 37
                    right - 39
                    space - 32
                    */
                    //Normal collision, e.g. platform, boundary, etc.
                    if (!specialCollision)
                    {
                        player.xSpeed = 0.0;
                        player.ySpeed = 0.0;
                        yAccel = 0.0;
                        xAccel = 0.0;
                        statistics.timerVal = 0;
                        player.kill(); //Enable again?
                    }
                }
            }
        }
        i++;
    }
    if (player.health <= 0) player.kill(); //Kill if dead. :P
}
