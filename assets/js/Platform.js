/*
    Each platform tile is an instance of this.

    @param ctx          The canvas context.
    @param img          The image object to draw.
    @param x            x-coordinate.
    @param y            y-coordinate.
    @param w            The width.
    @param h            The height.
*/
function Platform(ctx, img, x, y, w, h)
{
    this.ctx = ctx;
    this.img = img;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.damage = 100; //A platform collision will kill the player
    this.group = -1; //Represents an invalid group

    this.render = function()
    {
        this.ctx.drawImage(this.img,
            this.x,
            this.y,
            this.w,
            this.h);
    }
}
