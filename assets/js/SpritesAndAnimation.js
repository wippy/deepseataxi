/*
    The following code is mainly inspired by the the following tutorial presented by Steven Lambert:
    http://gamedevelopment.tutsplus.com/tutorials/an-introduction-to-spritesheet-animation--gamedev-13099

    A sprite is a single graphic image that is incorporated into a larger scene so that it appears to be part of the scene.
    When you put many sprites into a single image you get a spritesheet. Spritesheets are used to speed up the process of
    displaying images to the screen as it is much faster to fetch (ger: abrufen) one image and display only a single part
    of it than to fetch many images and display them.

    Mentioned this gives the idea of the following code. It uses one single image and displays only a single part of it; changing
    which frame is rendered in quick succession to give the illusion of movement. .. just remember back then: flicker book (ger: Daumenkino)

    The idea uses three parts of coding:
        1) Creating the image & calculate the number of rows and columns
        2) Updating the image to each frame of the animation
        3) Drawing the frame to the screen

    The genius thing about this code: We can define the animation sequence speed as well as the start and end frame
    of the spritesheet.
*/
/*
    ..creating the function that receives the Image()-Object that we can use to draw.
    Since different spritesheets can have different frame sizes, we'll need to pass
    the frame width and height so that we can accurately calculate how many frames are
    in a row and column of the image.
*/
function SpriteSheet(imageObject, frameWidth, frameHeight)
{
    //console.log("### START function SpriteSheet");
    this.image = imageObject; //--> imageAssets {}. It is the global object which contains all preloaded Image()-objects.
    this.frameWidth = frameWidth; // defined Width of each frame
    this.frameHeight = frameHeight; // defined Height of each frame

    //TODO : Deliver the name of the Image()-Object!
    /*
        //## DEBUG
        console.log(this.image);
        console.log("## DEBUG INFO..");
        console.log("Filepath: " + this.image.src);
        console.log("Image Width: " + this.image.width);
        console.log("Image Height: " + this.image.height);
        console.log("Frame Width: " + this.frameWidth);
        console.log("Frame Height: " + this.frameHeight);
    */
    /*

        ..calculate number of frames in a row and column
        NOTE: self is being used to maintain a reference to the original this even as the context is changing.
                --> this.image.width: context is imageObject itself outside the function
                --> self.frameWidth context refers to the attribute within the function

        You can get more information here: http://alistapart.com/article/getoutbindingsituations
    */
    var self = this;
    self.framesPerRow = Math.floor(this.image.width / self.frameWidth);
    self.rows = Math.floor(this.image.height / self.frameHeight);

    /*
        //## DEBUG
        console.log("Cols (Frames per Row): " + self.framesPerRow);
        console.log("Rows (Frames per Col): " + self.rows);
    */
    //console.log("### END function SpriteSheet");
}

//  an animation will be in charge..
function Animation(spritesheet, frameSpeed, startFrame, endFrame)
{
    //console.log("### START function Animation");
    var animationSequence = []; //will hold the order and amount of the animation frames
    var currentFrame = 0; //current frame to draw (within the animationSequence-Array)
    var counter = 0; //keeps track of the frame rate (game)

    /*
        //## DEBUG
        console.log("currentFrame: " + currentFrame);
        console.log("counter: " + counter);
        console.log("frameSpeed: " + frameSpeed);
        console.log("startFrame: " + startFrame);
        console.log("endFrame: " + endFrame);
    */

    /*

        ..how many frames does the animation of our spritesheet have?
        Get amount of frames and create fields within the array.

        Let us say we du have an image which is divided into frames like:
            1   2   3   4       --> could be a resting movement
            5   6   7   8       --> could be a movement L2R
            9   10  11  12      --> could be a movement R2L

        By defining and handing over startFrame and endFrame we are able do define
        the animation sequence itself within one single spritesheet. So if we want
        to make an animation Left-to-Right we simply us the frames 5,6,7 and 8.
        That`s it.. magic! :-)

    */
    for (var frameNumber = startFrame; frameNumber <= endFrame; frameNumber++)
    {
        animationSequence.push(frameNumber);
        /*
            //## DEBUG
            console.log(" puhsing frameNumber: " + animationSequence[frameNumber]);
        */
    }
    /*
        //## Debug
        console.log("Number of frames: " + frameNumber);
        console.log("Array animationSequence.length: " + animationSequence.length);
    */
    /*

        ..update to the next frame if it is time to do so.
        All we have to do is to change the frame (<-> section) of the spritesheet we will draw.

        At every frame of the game there will be an update of the spritesheet.
        !HOWEVER, the intention is NOT to switch to the next frame (image spritesheet)
        at every frame of the game. That is why the code calculates the amount of frames (game)
        to wait before transitioning. If we would not do so it would not be possible to
        adjust the animation sequence speed without reference to the framespeed of the game!

        By using the modulo (ger: Rest) operator (%) for the currentFrame, we can create a continuous
        loop every time the endFrame is reached, the currentFrame will revert back to 0,
        thus looping the animation.

        The attribute "frameSpeed" is defined by us and handed over. The higher the value
        the slower the animation sequence speed.
    */
    this.update = function()
    {
        /*


            min Value for frameSpeed is 1. This is logical since we want an animation sequence..
            frameSpeed => when (amount of frames (game) shall the transition take place).
            Update to the next frame if it is time to do so!
        */
        /*
            //## Debug
            if(counter == 0){
                console.log("frameSpeed of Sprite: " + frameSpeed);
                console.log("counter: " + counter);
            } else if(counter == (frameSpeed-1)){
                console.log("Time for frame update. Counter now: " + counter);
            }
        */
        if (counter == (frameSpeed - 1))
        {
            //default value currentFrame: "0"
            /*
                //## DEBUG
                console.log("Rechnung: " + (currentFrame+1) + " % " + animationSequence.length + " = " + ((currentFrame + 1) % animationSequence.length));
            */
            currentFrame = (currentFrame + 1) % animationSequence.length;
        }
        //update the counter. we already know the principle from the abovementioned calculation
        counter = (counter + 1) % frameSpeed;
    };

    this.draw = function(x, y)
    {
        //get the row and column of the frame we want to draw
        var row = Math.floor(animationSequence[currentFrame] / spritesheet.framesPerRow);
        var col = Math.floor(animationSequence[currentFrame] % spritesheet.framesPerRow);
        /*
            //## DEBUG
            console.log("Context: " + ctx);
            console.log("NAME SpriteSheet: " + spritesheet.name);
            console.log("Image.src SpriteSheet: " + spritesheet.image.src);
            console.log("Rechnung Row animationSequence[] / spritesheet.framesPerRow: " + animationSequence[currentFrame] + " / " + spritesheet.framesPerRow);
            console.log("Animation.DRAW current value: var row = " + row);
            console.log("Animation.DRAW current value: var col = " + col);
        */
        /*
            //## DRAW IMAGE
            ********************************************************************************
            * void ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight); *
            ********************************************************************************

            dx      - The X coordinate in the destination canvas at which to place the top-left corner of the source image.
            dy      - The Y coordinate in the destination canvas at which to place the top-left corner of the source image.
            dWidth  - The width to draw the image in the destination canvas. This allows scaling of the drawn image. If not specified, the image is not scaled in width when drawn.
            dHeight - The height to draw the image in the destination canvas. This allows scaling of the drawn image. If not specified, the image is not scaled in height when drawn.
            sx      - The X coordinate of the top left corner of the sub-rectangle of the source image to draw into the destination context.
            sy      - The Y coordinate of the top left corner of the sub-rectangle of the source image to draw into the destination context.
            sWidth  - The width of the sub-rectangle of the source image to draw into the destination context. If not specified, the entire rectangle from the coordinates specified by sx and sy to the bottom-right corner of the image is used.
            sHeight - The height of the sub-rectangle of the source image to draw into the destination context.
        */
        ctx.drawImage(spritesheet.image,
            col * spritesheet.frameWidth,
            row * spritesheet.frameHeight,
            spritesheet.frameWidth,
            spritesheet.frameHeight,
            x,
            y,
            spritesheet.frameWidth,
            spritesheet.frameHeight
        );

        /*
            //## DEBUG
            console.log("CTX.DRAWIMAGE WERTE: ");
            console.log("Spritesheet Name: " + spritesheet.name);
            console.log("SX: " + col * spritesheet.frameWidth);
            console.log("SY: " + row * spritesheet.frameHeight);
            console.log("sWidth: " + spritesheet.frameWidth);
            console.log("sHeight: " + spritesheet.frameHeight);
            console.log("dx: " + x);
            console.log("dy: " + y);
            console.log("dWidth: " + spritesheet.frameWidth);
            console.log("dHeight: " + spritesheet.frameHeight);
        */
    };

    this.hasEnded = function()
        {
            if (currentFrame == animationSequence.length - 1)
            {
                return 1;
            }
        }
        //console.log("### END function Animation");
}

//### Define the canvas in use..
var canvas = document.getElementById("game");

//Getting Context for the canvas
var ctx = canvas.getContext("2d");
