/*
    This is the Player.

    @param animations   An array containing all player animations.
    @param audio        All sounds for the player.
    @param explosion    An explosion instance for the player death.
    @param x            x-coordinate.
    @param y            y-coordinate.
    @param w            The width.
    @param h            The height.
*/
function Player(animations, audio, explosion, x, y, w, h)
{
    this.animations = animations;
    this._audio = audio;
    this.audio = [];
    this.isAudioPlaying = false;

    this.explosion = explosion;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.direction = 1;
    // Speed is how fast you are moving at the moment
    this.xSpeed = 0;
    this.ySpeed = 0;

    this.health = 100;
    this.dead = false;
    this.g = 0;

    //Hack for audio
    /*
        The new Audio() object (originally in this.audio) causes a reload after the sound
        stopped playing. Using a hardcoded <audio> and <source> tag in HTML we faced no further
        issues.
        To keep using the array, we re-create the audio object.
    */
    for (var i = 0; i < this._audio.length; i++)
    {
        this.audio[i] = document.createElement("audio");
        this.audio[i].src = this._audio[i].src;
        this.audio[i].volume = 0.6;
    }
    /*
        Change the direction based on the input.

        @param trigger    The direction to change to.
    */
    this.moveAnimation = function(trigger)
    {
        if (trigger != "sink")
        {
            this.audio[0].play();
        }
        switch (trigger)
        {
            case "left":
                this.direction = -2; // Left Direction Animation Sequence
                playerLastDirection = 1;
                break;
            case "right":
                this.direction = 2;
                playerLastDirection = 2;
                break;
            case "up":
                if (player.direction == -1 || player.direction == -2 || player.direction == -3)
                {
                    player.direction = -2;
                }
                else
                {
                    player.direction = 2;
                }
                playerLastDirection = 3;
                break;
            case "down":
                if (player.direction == -1 || player.direction == -2 || player.direction == -3)
                {
                    player.direction = -2;
                }
                else
                {
                    player.direction = 2;
                }
                playerLastDirection = 4;
                break;
            case "none":
                this.audio[0].pause();
                this.audio[0].currentTime = 0;
                this.audio[1].pause();
                this.audio[1].currentTime = 0;
                if (player.direction == 1 || player.direction == 2 || player.direction == 3)
                {
                    player.direction = 1;
                }
                else
                {
                    player.direction = -1;
                }
                break;
            case "sink":
                this.audio[1].play();
                if (this.direction == 1 || this.direction == 2 || this.direction == 3)
                {
                    this.direction = 3;
                }
                else
                {
                    this.direction = -3;
                }
                break;
        }
    }
    this.sinking = false;

    this.isSinking = function()
    {
        return (this.sinking) ? true : false;
    }

    //INFO: Animations contained [0] bis [5]: left / right; stationary, moving, sinking
    /*
        DEFINITION OF MOVEMENT:
        -1 SubMarineStatL   stationary, facing left       Frames 12-14      Element[0]
         1 SubMarineStatR   stationary, facing right      Frames 15-17      Element[1]
        -2 SubMarineMoveL   moving, facing left           Frames 0-5        Element[2]
         2 SubMarineMoveR   moving, facing right          Frames 6-11       Element[3]
        -3 SubMarineSinkL   sinking, facing left          Frames 18-20      Element[4]
         3 SubMarineSinkR   sinking, facing right         Frames 21-23      Element[5]
    */
    /*
        Draw the player at given coordinates.

        @param x            x-coordinate.
        @param y            y-coordinate.
    */
    this.draw = function(x, y)
        {
            if (!this.isDead())
            {
                switch (this.direction)
                {
                    case -1:
                        this.animations[0].draw(x, y);
                        break;
                    case 1:
                        this.animations[1].draw(x, y);
                        break;
                    case -2:
                        this.animations[2].draw(x, y);
                        break;
                    case 2:
                        this.animations[3].draw(x, y);
                        break;
                    case -3:
                        this.animations[4].draw(x, y);
                        break;
                    case 3:
                        this.animations[5].draw(x, y);
                        break;
                }
            }
        }
        //Update as long as the player is alive.
    this.update = function()
        {
            if (!this.isDead())
            {
                switch (this.direction)
                {
                    case -1:
                        this.animations[0].update();
                        break;
                    case 1:
                        this.animations[1].update();
                        break;
                    case -2:
                        this.animations[2].update();
                        break;
                    case 2:
                        this.animations[3].update();
                        break;
                    case -3:
                        this.animations[4].update();
                        break;
                    case 3:
                        this.animations[5].update();
                        break;
                }

                //For security
                this.updateHealth();
            }
        }
        //This will cause a chain reaction of actions regarding ending the game.
    this.kill = function()
    {
        this.health = 0;
        this.audio[0].pause();
        console.log("Player killed!");
        this.dead = true;
    }
    this.isDead = function()
        {
            return (this.dead) ? true : false;
        }
        //Draw the explosion at the last coordinates the player has been to
    this.explode = function()
        {
            this.explosion.x = this.x;
            this.explosion.y = this.y;
            this.explosion.render();
        }
        //Prevent life dropping below 0
    this.updateHealth = function()
    {
        if (this.health < 0)
        {
            this.health = 0;
        }
    }
}
