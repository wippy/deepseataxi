/*
    File: DeepSeaTaxi.js

    This is the core file of the game.
    It contains:
      - Init
      - The preloader
      - The gameloop

    Please make sure to load it first.
*/
//Ambient SFX Stuff
var ambientSFX;

//Game Interval variable
var gameInt;
var statistics;

/*
  Normal: 0
  Hard:   1
*/
var globalDifficulty = 0;

/*
    Author: PR

    gameAssets is a global object.
    It will contain all preloaded
    Image()-objects for easy access.
*/
var gameAssets = {};

//Setting asset paths
var backgroundImagePath = "./assets/img/bg/";
var trashImagePath = "./assets/img/trash_normal/25px/";
var soundPath = "./assets/sounds/";

//Setting PHP directory search paths
var phpBackgroundImagePath = "../img/bg/";
var phpTrashImagePath = "../img/trash_normal/25px/";
var phpSoundPath = "../sounds/";

//Contains nearly every object drawn on the map and you can probably collide with
var NPO = {};

//Create sprite variables
var OctopussySpr;
var PlantSpr;
var DiversSpr;
var FishesBigSpr;
var FishesSmallSpr;
var SeaWeedSprBig;
var SeaWeedSprSmall;
var YellowFish;

//Create player variables
var player;
var playerLastDirection = 0;

//Create gameloop variables
var playerOnMap = true;
var gameWon = false;

//Set a default map. Just in case.
var selectedMap = "level201.map";

var DST = {};

/*
    Author: PR

    DST.init will start the whole process to create the game.

    @param replay boolean which defines if we start a replay or a complete new load
*/
DST.init = function(replay)
{
    console.log("Initiating. Welcome!");

    //Since DST.init gets called after the level selection, we won't display the loader until this point
    document.getElementById("response-loader-holder").style.display = "block";

    //If init gets called for a replay, the containers aren't childs of the game-container and will fail.
    if (!replay)
    {
        document.getElementById("game-container").removeChild(document.getElementById("game-level-selection"));
        document.getElementById("game-container").removeChild(document.getElementById("difficulty-container"));
    }

    //Create the sub-arrays (and objects) of our preloaded asset storage
    gameAssets.backgrounds = [];
    gameAssets.trash = [];
    gameAssets.sprites = {};
    gameAssets.bling = [];
    gameAssets.sounds = {};

    //All assets we won't load dynamically.
    var neededAssets = [
    {
        "name": "box1Blue",
        "source": "./assets/img/tiles/box1Blue.png"
    },
    {
        "name": "boxLockedBlue",
        "source": "./assets/img/tiles/boxLockedBlue.png"
    },
    {
        "name": "box2Yellow",
        "source": "./assets/img/tiles/box2Yellow.png"
    },
    {
        "name": "boxLockedYellow",
        "source": "./assets/img/tiles/boxLockedYellow.png"
    },
    {
        "name": "box3Red",
        "source": "./assets/img/tiles/box3Red.png"
    },
    {
        "name": "boxLockedRed",
        "source": "./assets/img/tiles/boxLockedRed.png"
    },
    {
        "name": "box4Green",
        "source": "./assets/img/tiles/box4Green.png"
    },
    {
        "name": "boxLockedGreen",
        "source": "./assets/img/tiles/boxLockedGreen.png"
    },
    {
        "name": "box5Black",
        "source": "./assets/img/tiles/box5Black.png"
    },
    {
        "name": "boxLockedBlack",
        "source": "./assets/img/tiles/boxLockedBlack.png"
    },
    {
        "name": "box6Purple",
        "source": "./assets/img/tiles/box6Purple.png"
    },
    {
        "name": "boxLockedPurple",
        "source": "./assets/img/tiles/boxLockedPurple.png"
    },
    {
        "name": "castleLeft",
        "source": "./assets/img/tiles/castleLeft25px.png"
    },
    {
        "name": "castleMid",
        "source": "./assets/img/tiles/castleMid25px.png"
    },
    {
        "name": "castleRight",
        "source": "./assets/img/tiles/castleRight25px.png"
    },
    {
        "name": "castleCenter",
        "source": "./assets/img/tiles/castleCenter25px.png"
    },
    {
        "name": "castleAlone",
        "source": "./assets/img/tiles/castleAlone25px.png"
    },
    {
        "name": "edge",
        "source": "./assets/img/tiles/edge25px.png"
    },
    {
        "name": "exitBlank",
        "source": "./assets/img/tiles/exitBlank.png"
    },
    {
        "name": "exitClosed",
        "source": "./assets/img/tiles/exitClosed.png"
    },
    {
        "name": "arrowUp",
        "source": "./assets/img/tiles/arrowUp.png"
    },
    {
        "name": "arrowDown",
        "source": "./assets/img/tiles/arrowDown.png"
    },
    {
        "name": "arrowLeft",
        "source": "./assets/img/tiles/arrowLeft.png"
    },
    {
        "name": "arrowRight",
        "source": "./assets/img/tiles/arrowRight.png"
    },
    {
        "name": "signExit",
        "source": "./assets/img/tiles/signExit25px.png"
    },
    {
        "name": "platform",
        "source": "./assets/img/block.png"
    },
    {
        "name": "mineWithChainSmall",
        "source": "./assets/img/mineWith2Chain25x50.png"
    },
    {
        "name": "mineNoChain",
        "source": "./assets/img/mineNoChain.png"
    },
    {
        "name": "octo",
        "source": "./assets/img/Sprites/Octopus_noBG.png"
    },
    {
        "name": "octoSmall",
        "source": "./assets/img/Sprites/Octopus_noBG_25x25px.png"
    },
    {
        "name": "plant",
        "source": "./assets/img/Sprites/Plant_noBG.png"
    },
    {
        "name": "fishesBig",
        "source": "./assets/img/Sprites/FishesBig.png"
    },
    {
        "name": "fishesSmall",
        "source": "./assets/img/Sprites/FishesSmall.png"
    },
    {
        "name": "yellowFish",
        "source": "./assets/img/Sprites/YFSpriteFull.png"
    },
    {
        "name": "seaWeed38x50",
        "source": "./assets/img/Sprites/Seaweed_38x50_noBG.png"
    },
    {
        "name": "divers",
        "source": "./assets/img/Sprites/Divers_noBG.png"
    },
    {
        "name": "subMarine",
        "source": "./assets/img/Sprites/submarine60x38.png"
    },
    {
        "name": "subMarineKill",
        "source": "./assets/img/Sprites/submarineKill64x64.png"
    },
    {
        "name": "explodingMine",
        "source": "./assets/img/Sprites/explodingMine50x50.png"
    },
    {
        "name": "blingbling",
        "source": "./assets/img/goldskull.png"
    },
    {
        "name": "subMarineKill",
        "source": "./assets/sounds/subMarineKill.mp3"
    },
    {
        "name": "subMarinePropeller",
        "source": "./assets/sounds/subMarinePropeller.mp3"
    },
    {
        "name": "subMarineBeep",
        "source": "./assets/sounds/subMarineSinkBeep.mp3"
    },
    {
        "name": "heyYou",
        "source": "./assets/sounds/heyYou.mp3"
    },
    {
        "name": "OctopussyPain",
        "source": "./assets/sounds/painOctopussy.mp3"
    },
    {
        "name": "underwaterAmbience1min",
        "source": "./assets/sounds/underwaterAmbience1min.mp3"
    },
    {
        "name": "underwaterMineExplosion",
        "source": "./assets/sounds/underwaterMineExplosion.mp3"
    }];


    //Init all NPO subarrays
    NPO.boundaries = [];
    NPO.platforms = [];
    NPO.enemies = [];
    NPO.plants = [];
    NPO.mines = [];
    NPO.entities = []; //Generic blocks: Trash, Exit Sign
    NPO.explosions = [];
    NPO.divers = [];
    NPO.exit = [];

    //Create a basic instance of statistics. It gets initialized after player creation.
    statistics = new Statistics(globalDifficulty);

    //Start the preloader.
    this.preloadAssets(neededAssets, gameAssets, this.create);
};

/*
    DST.preloadAssets will request the given assets from the server
    and load them into our storage object.

    @param files      All not dynamically (PHP) loaded assets.
    @param storage    Will contain all assets later.
    @param callback   Function to get called after finishing loading.
*/
DST.preloadAssets = function(files, storage, callback)
{
    console.log("Preloader started.");
    var currentAudio; //Current audio object we are preloading.
    var currentImage; //Current Image object we are preloading.
    var assetPathCollection = []; //Contains all asset paths to have one collection to iterate through.
    var remainingAssets = 0; //The obligatory counter to determine when to call the callback.

    console.log("Retrieving dynamic Image List..");

    //Preload using AJAX.
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
    {
        if (xhttp.readyState == 4 && xhttp.status == 200)
        {
            //Retrieve response and parse it into a readable object.
            console.log("Dynamic Image List retrieved.");
            var dynImageList = JSON.parse(xhttp.responseText);

            //Iterate through all backgrounds and add these to the path collection.
            for (var i = 0; i < dynImageList[0].length; i++)
            {
                assetPathCollection.push(backgroundImagePath + dynImageList[0][i].name);
            }
            //Iterate through all trash assets and add these to the path collection.
            for (var i = 0; i < dynImageList[1].length; i++)
            {

                assetPathCollection.push(trashImagePath + dynImageList[1][i].name);
            }
            //Iterate through the "hard coded" assets and add them to the path collection.
            for (var i = 0; i < files.length; i++)
            {
                assetPathCollection.push(files[i].source);
            }

            remainingAssets = assetPathCollection.length;

            //Preload every single given asset.
            for (var i = 0; i < remainingAssets; i++)
            {
                console.log("Loading " + assetPathCollection[i]);
                //Check if the asset is an audio asset...
                if (assetPathCollection[i].indexOf(soundPath) > -1)
                {
                    currentAudio = new Audio(); //Create a new Audio object for every iteration - Every sound

                    //Audio elements don't have an onload listener.
                    currentAudio.addEventListener("canplaythrough", audioLoaded, false);
                    currentAudio.src = assetPathCollection[i];

                    function audioLoaded()
                    {
                        --remainingAssets;

                        //Check if we loaded the last asset.
                        if (remainingAssets <= 0)
                        {
                            console.log("Preloading done.");

                            //Callback
                            callback();
                        }
                    }
                }
                else
                { //...or an image
                    currentImage = new Image(); //Create a new Image object for every iteration - Every image.

                    //Event listener if the image is fully loaded.
                    currentImage.onload = function()
                    {
                        --remainingAssets;

                        //Check if we loaded the last asset.
                        if (remainingAssets <= 0)
                        {
                            console.log("Preloading done.");

                            //Callback
                            callback();
                        }
                    };
                    //Retrieve the current object source from our collection.
                    currentImage.src = assetPathCollection[i];
                }

                //Push the current object to the fitting storage sub-array depending on which type of object it is.
                if (assetPathCollection[i].indexOf(backgroundImagePath) > -1)
                {
                    storage.backgrounds.push(currentImage);
                }
                else if (assetPathCollection[i].indexOf(trashImagePath) > -1)
                {
                    storage.trash.push(currentImage);
                }
                else if (assetPathCollection[i].indexOf("goldskull") > -1) // DO NOT RENAME FILE
                {
                    storage.bling.push(currentImage);
                }
                else if (assetPathCollection[i].indexOf(soundPath) > -1)
                {
                    /*
                      assetPathCollection is longer than files (hardcoded), because it contains
                      the dynamic list from PHP.
                      To retrieve only the hardcoded assets, we have to substract the length
                      of our dynamic files from the current index.
                    */
                    var t = (i - (dynImageList[0].length + dynImageList[1].length));
                    //The sound (& sprites) storage is an object - Pushing an object differs from arrays.
                    storage.sounds[files[t].name] = currentAudio;
                }
                else
                {
                    var t = (i - (dynImageList[0].length + dynImageList[1].length));
                    storage.sprites[files[t].name] = currentImage;
                }
            }
        }
    };

    //Define the files we want to preload dynamically with PHP.
    var listBGObj = {
            "types": [
            {
                "path": phpBackgroundImagePath,
                "filetype": "jpg"
            },
            {
                "path": phpTrashImagePath,
                "filetype": "png"
            }]
        }
        //Convert into JSON
    var listStrData = JSON.stringify(listBGObj);
    xhttp.open("POST", "././assets/php/DynamicImageListRequestHandler.php", true);
    xhttp.send(listStrData);
};
/*
    DST.create initializes most of the needed base objects (sprites / player).
    Besides initializing our statistics, too, we re-init all non-object-function vars
    to support the replay of a map.
*/
DST.create = function()
{
    console.log("Building...");
    /*
        This will initialize the spritesheets and start the (test) animation.
        Since different spritesheets can have different frame sizes, we'll need to pass
        the frame width and height so that we can accurately calculate how many frames are
        in a row and column of the image.
    */

    /** ====== Sprites ====== **/
    /* Actually used */
    PlantSpr = new SpriteSheet(gameAssets.sprites.plant, 50, 50);
    DiversSpr = new SpriteSheet(gameAssets.sprites.divers, 25, 25);
    SeaWeedBigSpr = new SpriteSheet(gameAssets.sprites.seaWeed38x50, 38, 50);
    SubMarineSpr = new SpriteSheet(gameAssets.sprites.subMarine, 60, 38);
    SubMarineKillSpr = new SpriteSheet(gameAssets.sprites.subMarineKill, 64, 64);
    ExplodingMineSpr = new SpriteSheet(gameAssets.sprites.explodingMine, 50, 50);
    OctopussySpr = new SpriteSheet(gameAssets.sprites.octo, 35, 38);


    /** ====== Animations ====== **/
    /* Actually used */
    SeaWeedBigMove = new Animation(SeaWeedBigSpr, 8, 0, 15);
    PlantMove = new Animation(PlantSpr, 12, 0, 4);
    //SUBMARINE ergo PLAYER Movement Animations
    SubMarineMoveL = new Animation(SubMarineSpr, 3, 0, 5);
    SubMarineMoveR = new Animation(SubMarineSpr, 3, 6, 11);
    SubMarineStatL = new Animation(SubMarineSpr, 3, 12, 14);
    SubMarineStatR = new Animation(SubMarineSpr, 3, 15, 15);
    SubMarineSinkL = new Animation(SubMarineSpr, 6, 18, 20);
    SubMarineSinkR = new Animation(SubMarineSpr, 6, 21, 23);

    OctoMoveRtoL = new Animation(OctopussySpr, 6, 8, 16);
    OctoMoveLtoR = new Animation(OctopussySpr, 6, 29, 39);

    DiverBlueMove = new Animation(DiversSpr, 6, 0, 2);
    DiverGreenMove = new Animation(DiversSpr, 6, 18, 20);
    DiverRedMove = new Animation(DiversSpr, 6, 3, 5);
    DiverOrangeMove = new Animation(DiversSpr, 6, 72, 74);
    DiverYellowMove = new Animation(DiversSpr, 6, 33, 35);
    DiverBlackMove = new Animation(DiversSpr, 6, 93, 95);

    //Create PLAYER object.. definition of animations via elements [0]-[5]
    player = new Player([SubMarineStatL, SubMarineStatR, SubMarineMoveL, SubMarineMoveR, SubMarineSinkL, SubMarineSinkR], [gameAssets.sounds.subMarinePropeller, gameAssets.sounds.subMarineBeep], new Explosion(new Animation(SubMarineKillSpr, 4, 0, 15),
        gameAssets.sounds.subMarineKill, 400, 400, 64, 64), 400, 400, 60, 38);

    //Create the ambient sound object.
    ambientSFX = document.createElement("audio");
    ambientSFX.src = gameAssets.sounds.underwaterAmbience1min.src;
    ambientSFX.loop = true;
    ambientSFX.volume = 1.0;

    statistics.init();

    /*
      To support the replay of a map, we have to re-init
      all variables which are not set by objects.
    */
    initKeyParam();
    initPhysics();
    initCollisionParam();
    initWorldParam();

    //Start level loading
    loadLevel(selectedMap);
};
/*
    DST.start will mainly show the canvas and start the game loop.
*/
DST.start = function()
{
    document.getElementById("response-loader-holder").style.display = "none";
    document.getElementById("how-to-play").style.display = "block";
    document.getElementById("game").style.display = "inline";
    document.getElementById("game-bar").style.display = "block";
    document.getElementById("game").focus(); //Keylistener only set to canvas, so focus it.
    document.getElementById("game-bar").scrollIntoView();
    //The setInterval is a quick way to write a game loop. The function calls the game loop 30 times per second.
    gameInt = setInterval(function()
    {
        DST.game();
    }, 1000 / 30); //30 FPS

    ambientSFX.play();
    statistics.startTimer();
}

/*
    DST.game is the main gameloop and handles the options to stop
    the game as well as calling the logic and render updates.
*/
DST.game = function()
{
    //When successfully left through an exit.
    if (gameWon)
    {
        statistics.stopTimer();
        this.endGame(true);
    }
    //When dead.
    if (player.isDead())
    {
        statistics.stopTimer(); //Clear timer first, otherwise it will keep running

        //Make sure that we aren't already exploding
        if (!player.explosion.isExploding())
        {
            player.explosion.shouldExplode = true;
        }
        //The gameloop will continue as long as we are exploding.
        if (player.explosion.getAnimation().hasEnded())
        {
            player.explosion.shouldExplode = false;
            //Stop all player SFX as soon as Exlosion-Animation ends
            for (i = 0; i < player.audio.length; i++)
            {
                player.audio[i].pause();
            }
            this.endGame(false);
        }
    }
    this.update(); //Logic update
    this.render(); //Render update
};
//DST.update updates the collision as well as the movement as long as the player isn't dead.
DST.update = function()
{
    if (!player.isDead())
    {
        movement();
        collision();
    }
}

DST.render = function()
    {
        updateMap();

        /*
            Author: SK

            Within the following lines the methods for each sprite object are called to
            update and draw the associated frame. The method update() calculates the frame needed
            for a nice and smooth motion sequence. The draw(x,y) method then draws the current frame of the
            associated frame onto the canvas section [x, y are attributes to position the frame inside
            the coordinate system].
        */

        //We just need one update for all Animations
        OctoMoveRtoL.update();
        OctoMoveLtoR.update();

        SeaWeedBigMove.update();

        player.update();
        player.draw(player.x, player.y);

        statistics.update();
    }
    /*
        DST.endGame will stop the gameloop and update
        the statistics one last time.
        The

        @param won    If we have won the game or died.
    */
DST.endGame = function(won)
{
    ambientSFX.pause();
    console.log("Stopping game..");

    clearInterval(gameInt); //Stop game loop.
    statistics.setScore(statistics.getTimeLeft() + player.health); //Calculate the endscore.

    console.log("Game stopped. It's been fun.");

    //Form Stuff
    document.getElementById("form-game-send-highscore-head").innerHTML = (won) ? "CONGRATULATIONS!" : "GAME OVER";
    if (won) document.getElementById("form-game-send-highscore").style.marginTop = "-350px"; //If you won the game, the skull is missing, so the form can be displayed higher.
    document.getElementById("form-game-send-highscore-score").value = Math.floor(statistics.getScore());
    document.getElementById("form-game-send-highscore-level").value = statistics.getLevel();
    document.getElementById("game").className += " game-finished"; //BLUUUUUR
    document.getElementById("form-game-send-highscore").style.visibility = "visible";
}
