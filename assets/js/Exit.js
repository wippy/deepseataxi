/*
    This file handles the exit(s).

    @param ctx          The canvas context.
    @param img          The image objects to draw, based on the exit type..
    @param x            x-coordinate.
    @param y            y-coordinate.
    @param w            The diver width.
    @param h            The diver height.
    @param i            The tile row in the two-dimensional map representation.
    @param j            The tile column in the two-dimensional map representation.
    @param difficulty   The global difficulty value to determine damage. (Not needed.)

*/
function Exit(ctx, img, x, y, w, h, i, j, difficulty)
{
    this.ctx = ctx;
    this.img = img;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.row = i;
    this.col = j;
    this.difficulty = difficulty;
    this.exitType = 0;

    this.damage = (this.difficulty) ? 100 : 50;

    this.render = function()
    {
        //CHeck if divers left (locked) or not (unlocked).
        if (NPO.divers.length > 0) this.exitType = 0; //Divers on map. Exit is closed!
        if (NPO.divers.length <= 0)
        {
            if (this.row == 0) this.exitType = 2; //Top Exit
            else if (this.row == 23) this.exitType = 3; //Bottom Exit
            else if (this.col == 0) this.exitType = 4; //Left Exit
            else if (this.col == 31) this.exitType = 5; //Right Exit

        }

        this.ctx.drawImage(this.img[this.exitType],
            this.x,
            this.y,
            this.w,
            this.h);
    }
}
