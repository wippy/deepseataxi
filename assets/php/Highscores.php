<?php
    class Highscores
    {
        /*
            getHighscores returns a table with the best 10
            highscores.
        */
        static function getHighscores()
        {
            require_once "Database.php";
            try {
                $stmt = $con->prepare("SELECT * FROM Highscores ORDER BY highscoreScore DESC LIMIT 10"); //Prepare the select
                $ok = $stmt->execute();
                //Everything went ok?
                if(!$ok)
                {
                    echo '<p class="under-water-small">No Highscores so far.</p>';
                    exit;
                } else {
                      $result = $stmt->fetchAll();
                      if(count($result)<=0)
                      {
                        echo '<p class="under-water-small">No Highscores so far.</p>';
                        exit;
                      } else {
                        $resultTable =
                        "<table>
                        <thead>
                        <th>Punkte</th>
                        <th>Spieler</th>
                        <th>Map</th>
                        <th>Datum</th>
                        </thead>
                        <tbody>";
                        //A new row for every entry.
                        foreach($result as $row)
                        {
                            $resultTable .= "<tr>
                                <td>" . $row['highscoreScore']."</td>
                                <td>" . $row['playerName']."</td>
                                <td>" . $row['mapName'] . "</td>
                                <td>" . ((empty($row['gameDate'])) ? "N/A" : $row['gameDate']) . "</td></tr>";
                        }

                        $resultTable .= "</tbody></table>";

                        echo $resultTable;
                        exit;
                    }
                }
            } catch( Exception $ex ) {
			            die('There was an error running the query [' . $ex->getMessage() . ']<br><br>');
            }
        }
        /*
            setHighscore inserts a new data row into the db.

            @param $data    The Highscore form post data.
        */
        static function setHighscores($data)
        {
            //Set anonymous if no name was submitted.
            $playerName = (empty(trim($data['form-game-send-highscore-name']))) ? "Anonymous" : $data['form-game-send-highscore-name'];
            $playerScore = $data['form-game-send-highscore-score'];
            $playerMap = $data['form-game-send-highscore-level'];

            require_once "Database.php";
            try {
                //Prepare the statement and bind the values.
                $stmt = $con->prepare("INSERT INTO Highscores (highscoreScore, playerName, mapName, gameDate) VALUES (:score, :name, :map, :gameDate)");
                $stmt->bindParam(':score', $playerScore);
                $stmt->bindParam(':name', $playerName);
                $stmt->bindParam(':map', $playerMap);
                $stmt->bindParam('gameDate', date('Y-m-d'));

                $ok = $stmt->execute();

                if(!$ok)
                {
                    die(var_dump($stmt));
                    exit;
                } else {
                    header("Location: ../../highscores.html?f=hs"); //Redirect if everything went fine.
                    exit;
                }
            } catch( Exception $ex)
            {
                die('There was an error running the query [' . $ex->getMessage() . ']<br><br>');
                exit;
            }
        }
    }
?>
