<?php
    class ImageList
    {
        /*
            listImage returns an object with filenames,
            retrieved by searching through a given Directory
            with a given search pattern.

            @param $pathList       Contains paths and filetypes (pattern)
        */
        static function listImages($pathList)
        {
            //Supports different paths.
            for($i = 0; $i < count($pathList['types']); $i++)
            {
                $k = 0;
                $imageList[$i] = array(); //Create a new subarray for every new directory.
                //Glob searches a given directory with a given pattern (filetype).
                foreach(glob($pathList['types'][$i]['path'].'*.'.$pathList['types'][$i]['filetype']) as $filename){
                    $imageList[$i][$k] = array('name' => basename($filename)); //Create a new entry for every image found.
                    $k++;
                }
            }
            echo json_encode($imageList);
        }
    }
?>
