<?php
    //This file handles the actual request and calls the ImageList class.
    require_once "DynamicImageList.php";
    $data = json_decode(file_get_contents('php://input'),true); //Decode input data
    ImageList::listImages($data);
?>
