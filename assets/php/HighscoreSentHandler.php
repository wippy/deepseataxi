<?php
//Handles the highscore request from highscores.html
/*
  I changed the detection for a POST event,
  because of the javascript submit, the
  submit button exists no longer.
*/
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    require_once 'Highscores.php';
    Highscores::setHighscores($_POST);
    exit;
} else
{
    header("Location: ../../index.php");
    exit;
}
?>
