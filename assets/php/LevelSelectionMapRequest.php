<?php
    $mapPath = "../maps/"; //Relative to PHP
    $k = 0;
    $mapList = array();

    //Search a given directory with a given pattern (.map) and return all files.
    foreach(glob($mapPath.'*.map', GLOB_BRACE) as $filename)
    {   //Check if a thumbnail exists.
        if(file_exists($mapPath . "" . basename($filename) . ".jpg"))
        {
            $mapList[$k] = array(
                'map'   => basename($filename),
                'thumb' => basename($filename) . ".jpg"); //Insert the map name and the thumb name.
        } else
        {
            $mapList[$k] = array('map' => basename($filename),'thumb'=>"none"); //If no thumbe exists, enter §none".
        }

        $k++;
    }
    echo json_encode($mapList);
?>
