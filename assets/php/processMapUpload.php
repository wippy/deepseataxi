<?php
    /*
        Okay. Let me clear up a few things:

        http://whocares.de/php-fix-empty-post-array/
        I can't get my data using $_POST. Why? It's empty.
        Finally, after hours of summoning Google, I found the above site.

        Their trick explained the issue well and their test worked.
        We are using it now as a workaround. Because try to reconfigure a
        PHP installation on a shared hoster...

        Anyway, firstly, we decode the JSON encoded data (JSON.stringify in JS),
        and then use the input stream to get our data from PHP.

        The boolean, true, defines, that json_decode should return an associative
        array.
    */
    $data = json_decode(file_get_contents('php://input'),true);

    //Map name in the data set?
    if(isset($data['map_name']))
    {
        //Write map to file.
        file_put_contents('../maps/'. $data['map_name'], $data['map_data']);
        //Remove the uneeded string in the data stream.
        $im = str_replace('data:image/jpeg;base64,','',$data['thumb']);
        //Write the image data after decoding to file.
        file_put_contents('../maps/'. $data['map_name'].'.jpg', base64_decode($im));
        echo json_encode(array('success' => "1")); //Return a success code.
    } else {
        echo json_encode(
            array(
                'success' => '-1',
                'data' => $data
                )
            );
    }
?>
