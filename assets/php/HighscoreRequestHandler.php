<?php
    $data = json_decode(file_get_contents('php://input'),true); //Decode input data
    //If header is set correctly.
    if(isset($data['getHighscores']) && $data['getHighscores'])
    {
        require_once "Highscores.php";
        Highscores::getHighscores();
        exit;
    } else {
        //Redirect if browsed directly.
        header("Location: ../../index.php");
        exit;
    }
?>
