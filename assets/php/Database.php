<?php
try {
		//Create a new sqlite database object with the given file. Create if not exists.
		$con = new PDO('sqlite:../db/dst.sqlite3');

		//Set error handling stuff.
		$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$con->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);

		//Prepare the table creation, only happens if it doesn't exist.
        $stmt = $con->prepare("CREATE TABLE IF NOT EXISTS Highscores
                (highscoreID INTEGER PRIMARY KEY,
                highscoreScore INTEGER,
                playerName TEXT,
                mapName TEXT,
                gameDate TEXT)");
        $stmt->execute();
	} catch (Exception $ex) {
			die("Exception: " . $ex->getMessage());
	}
?>
