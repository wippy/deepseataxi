<!doctype html>
<html lang="en">
  <head>
    <title>WIPpy - Deep Sea Taxi</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/css/deepseataxi.css">
    <link rel="stylesheet" href="assets/css/loader.css">
  </head>
  <body>
    <h1 class="under-water">Deep Sea Taxi</h1>
    <h3 class="under-water-small">The wettest Space Taxi ever!</h3>
    <div id="navigation">
        <span><a href="./" title="Start" alt="Start">Start</a></span>
        <span><a href="./play.html" title="Play the Game" alt="Play the Game">Play the Game</a></span>
        <span><a href="./highscores.html" title="Highscores" alt="Highscores">Hall of Fame</a></span>
        <span><a href="./mapupload.html" title="Mapupload" alt="Mapupload">Mapupload</a></span>
        <span><a href="http://wippy.bplaced.net" title="Development Blog (German)" alt="Development Blog (German)">Dev Blog</a></span>
    </div>
    <hr id="hr-head-border">
      <p id="index-content-container" class="under-water-small">Have a look at this study project realised by Phillip R., David S. and Stephan K. by diving with us down into the deep blue sea..</p>
      <img src="assets/img/Titel.png" width="600" height="450" alt="Titelbild"></img>
  </body>
</html>
